#include <glados/Singleton.h>
#include <glados/Singleton_API.h>
#include <typeindex>
//#include <typeinfo>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <utility>

namespace
{
struct SingleTonHolder
{
	void* m_object{nullptr};
	std::shared_ptr<std::mutex> m_mutex;
};
}

// Global mutex
static auto getSingleTonMutex() -> std::mutex&
{
	// s_singleTonMutex is not 100% safety for multithread
	// but if there's any singleton object used before thread, it's safe enough.
	static std::mutex s_singleTonMutex;
	return s_singleTonMutex;
}

static auto getSingleTonType(const std::type_index& typeIndex) -> SingleTonHolder*
{
	static std::unordered_map<std::type_index, SingleTonHolder> s_singleObjects;

	// Check the old value
	auto itr = s_singleObjects.find(typeIndex);
	if(itr != s_singleObjects.end())
	{
		return &itr->second;
	}

	// Create new one if no old value
	std::pair<std::type_index, SingleTonHolder> const singleHolder(typeIndex, SingleTonHolder());
	itr = s_singleObjects.insert(singleHolder).first;
	SingleTonHolder& singleTonHolder = itr->second;
	singleTonHolder.m_object = nullptr;
	singleTonHolder.m_mutex = std::make_shared<std::mutex>();

	return &singleTonHolder;
}

SINGLETON_API auto getSharedInstance(
	const std::type_index& typeIndex, void* (*getStaticInstance)(), void*& instance) -> void
{
	// Get the single instance
	SingleTonHolder* singleTonHolder = nullptr;
	{
		// Locks and get the global mutex
		const std::lock_guard<std::mutex> myLock(getSingleTonMutex());
		if(instance != nullptr)
		{
			return;
		}

		singleTonHolder = getSingleTonType(typeIndex);
	}

	// Create single instance
	{
		// Locks class T and make sure to call construction only once
		const std::lock_guard<std::mutex> myLock(*singleTonHolder->m_mutex);
		if(singleTonHolder->m_object == nullptr)
		{
			// construct the instance with static funciton
			singleTonHolder->m_object = (*getStaticInstance)();
		}
	}

	// Save single instance object
	{
		std::lock_guard<std::mutex> myLock(getSingleTonMutex());
		instance = singleTonHolder->m_object;
	}
}
