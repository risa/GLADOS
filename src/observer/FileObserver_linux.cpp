// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <glados/Utils.h>
#include <glados/observer/FileObserver.h>

#include <array>
#include <fstream>
#include <poll.h>
#include <sys/stat.h>
#include <unistd.h>
#include <bit>

namespace glados
{

auto FileObserver::init(
	glados::Observer* observer, std::string filePath, const std::string& fileName, uint32_t inotifyEvents) -> void
{
	attach(observer);
	m_initOk = setFilePathToObserve(std::move(filePath), fileName);
	setEventsToObserve(inotifyEvents);
}

auto FileObserver::attach(glados::Observer* observer) -> void { m_observers.push_back(observer); }

auto FileObserver::detach(glados::Observer* observer) -> void { m_observers.remove(observer); }

auto FileObserver::notify() -> void
{
	for(auto& observer : m_observers)
	{
		observer->update(this);
	}
}

auto FileObserver::setFilePathToObserve(std::string filePath, const std::string& fileName) -> bool
{

	if(filePath.back() != '/')
	{
		filePath.push_back('/');
	}

	try
	{

		if(!glados::utils::FileSystemHandle::createDirectory(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access path to observe '" << filePath
									 << "'.";
			return false;
		}

		// check if file to observe exists
		std::string fullPath = filePath + fileName;
		if(!glados::utils::FileSystemHandle::createEmptyFile(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access file to observe '" << fullPath
									 << "'.";
			return false;
		}

		if(filePath != "/tmp/" && !glados::utils::FileSystemHandle::setFilePermissionsWriteAll(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration directory '"
				<< filePath << "'.";
			return false;
		}

		if(!glados::utils::FileSystemHandle::setFilePermissionsWriteAll(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration file'"
				<< fullPath << "'.";
			return false;
		}

		// initialize inotify
		m_inotifyInstance = inotify_init();
		if(m_inotifyInstance < 0)
		{
			// something wrong with inotify
			return false;
		}
	}
	catch(std::runtime_error const& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Error when initializing FileObserver: "
								 << e.what();
		return false;
	}
	catch(...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Unknown error when initializing FileObserver.";
		return false;
	}

	m_fileName = fileName;
	m_filePath = filePath;
	m_fileContentsValid = false;
	return true;
}

auto FileObserver::setEventsToObserve(uint32_t inotifyEvents) -> void { m_observedEvents = inotifyEvents; }

auto FileObserver::start() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Starting to observe \"" << m_filePath << m_fileName
							 << "\"";
	std::future<void> stopSigFuture = m_stopSig.get_future();
	m_observerThread = std::thread{&FileObserver::observe, this, std::move(stopSigFuture)};
}

auto FileObserver::wait() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Requesting to stop...";
	m_stopSig.set_value();
	m_observerThread.join();
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopped.";
}

auto FileObserver::observe(std::future<void> stopSig) -> void
{
	m_watchDescriptor = inotify_add_watch(m_inotifyInstance, m_filePath.c_str(), m_observedEvents);
	constexpr const size_t timeout_ms{50};
	std::array<char, m_EVENT_BUF_LEN> buffer{};
	ssize_t idx = 0;
	ssize_t	length = 0;

	while(true)
	{
		struct pollfd pfd = {m_inotifyInstance, POLLIN, 0};
		int ret = poll(
			&pfd, 1, timeout_ms); // poll for file changes every 50 ms -> allows us to break in between polls

		if(ret > 0)
		{
			length = read(m_inotifyInstance, buffer.data(), m_EVENT_BUF_LEN);

			if(length < 0)
			{
				BOOST_LOG_TRIVIAL(error) << "glados::FileObserver:: Error when reading inotify instance.";
				return;
			}
			idx = 0;
			// clang-tidy doesn't seem to understand the loop for some reason
			// NOLINTBEGIN
			while(idx < length)
			{
				#if __cpp_lib_bit_cast
				auto* event = std::bit_cast<struct inotify_event*>(&(buffer.at((size_t)idx)));
				#else
				auto* event = (struct inotify_event*)&buffer.at((size_t)idx);
				#endif
				if((event != nullptr) && (event->len != 0))
				{
					if((event->mask & m_observedEvents) != 0)
					{
						if((event->mask & IN_ISDIR) == 0)
						{
							const std::string changedFile(&(event->name[0]));
							if(changedFile == m_fileName)
							{
								m_message = "File changed!";
								std::ifstream fstream(m_filePath + m_fileName, std::ios::binary | std::ios::ate);
								std::streamsize size = fstream.tellg();
								fstream.seekg(0, std::ios::beg);
								m_fileContents.resize((size_t)size);
								if(size > 0 && fstream.read(m_fileContents.data(), size))
								{
									m_fileContentsValid = true;
									notify();
								}
								else
								{
									m_message = "Cannot read file contents!";
									m_fileContentsValid = false;
									notify();
								}
							}
						}
					}
				}
				idx += (ssize_t)(m_EVENT_SIZE + event->len);
			}
			// NOLINTEND
		}

		if(stopSig.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
		{
			break;
		}
	}

	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopping <" << m_fileName << ">";
	inotify_rm_watch(m_inotifyInstance, m_watchDescriptor);
	close(m_inotifyInstance);
}

}
