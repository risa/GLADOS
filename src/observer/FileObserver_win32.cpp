// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <glados/Utils.h>
#include <glados/observer/FileObserver.h>

#include <codecvt>
#include <fstream>

namespace glados
{
auto FileObserver::init(
	glados::Observer* o, std::string filePath, const std::string& fileName, uint32_t inotifyEvents) -> void
{
	attach(o);
	setEventsToObserve(inotifyEvents);
	m_initOk = setFilePathToObserve(std::move(filePath), fileName);
};

auto FileObserver::attach(glados::Observer* o) -> void { m_observers.push_back(o); }

auto FileObserver::detach(glados::Observer* o) -> void { m_observers.remove(o); }

auto FileObserver::notify() -> void
{
	for(auto& o : m_observers)
		o->update(this);
}

auto FileObserver::setFilePathToObserve(std::string filePath, std::string fileName) -> bool
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver: Setup path for " << fileName << " in " << filePath;

	FORMAT_PATH(filePath);

	try
	{

		if(!glados::utils::FileSystemHandle::createDirectory(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access path to observe '" << filePath
									 << "'.";
			return false;
		}

		// check if file to observe exists
		std::string fullPath = filePath + fileName;
		if(!glados::utils::FileSystemHandle::createEmptyFile(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access file to observe '" << fullPath
									 << "'.";
			return false;
		}

		if(filePath != "C:/temp/" && !glados::utils::FileSystemHandle::setFilePermissionsWriteAll(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration directory '"
				<< filePath << "'.";
			return false;
		}

		if(!glados::utils::FileSystemHandle::setFilePermissionsWriteAll(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration file'"
				<< fullPath << "'.";
			return false;
		}

		// initialize
		m_directoryHandle = CreateFile(filePath.c_str(), FILE_LIST_DIRECTORY,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING,
			FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, NULL);

		if(m_directoryHandle == NULL)
		{
			return false;
		}

		m_overlapped.hEvent = CreateEvent(NULL, FALSE, 0, NULL);
	}
	catch(std::runtime_error const& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Error when initializing FileObserver: "
								 << e.what();
		return false;
	}
	catch(...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Unknown error when initializing FileObserver.";
		return false;
	}

	m_fileName = fileName;
	m_filePath = filePath;
	m_fileContentsValid = false;
	return true;
}

auto FileObserver::setEventsToObserve(uint32_t inotifyEvents) -> void { m_observedEvents = inotifyEvents; }

auto FileObserver::start() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Starting to observe \"" << m_filePath << m_fileName
							 << "\"";
	std::future<void> stopSigFuture = m_stopSig.get_future();
	m_observerThread = std::thread{&FileObserver::observe, this, std::move(stopSigFuture)};
}

auto FileObserver::wait() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Requesting to stop...";
	m_stopSig.set_value();
	m_observerThread.join();
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopped.";
}

auto FileObserver::observe(std::future<void> stopSig) -> void
{

	auto utf8_encode = [](const std::wstring& wstr) -> std::string
	{
		if(wstr.empty())
			return std::string();
		int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
		std::string strTo(size_needed, 0);
		WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
		return strTo;
	};

	while(true)
	{
		if(!ReadDirectoryChangesW(
			   m_directoryHandle, m_changesBuf, 1024, false, m_observedEvents, NULL, &m_overlapped, NULL))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver: Unable to setup directory monitoring.";
			return;
		}
		m_dwWaitStatus = WaitForSingleObject(m_overlapped.hEvent, 50);

		switch(m_dwWaitStatus)
		{
		case WAIT_OBJECT_0:
		{
			DWORD bytes_transferred;
			GetOverlappedResult(m_directoryHandle, &m_overlapped, &bytes_transferred, FALSE);
			FILE_NOTIFY_INFORMATION* event = (FILE_NOTIFY_INFORMATION*)m_changesBuf;

			while(true)
			{
				DWORD name_len = event->FileNameLength / sizeof(wchar_t);
				switch(event->Action)
				{
				case FILE_ACTION_MODIFIED:
				{
					std::wstring tmp(event->FileName, name_len);

					std::string changedFileName = utf8_encode(tmp);

					if(changedFileName == m_fileName)
					{
						m_message = "File changed! (" + m_filePath + m_fileName + ")";
						std::ifstream fs(m_filePath + m_fileName, std::ios::binary | std::ios::ate);
						if(fs.good())
						{
							fs.seekg(0, std::ios::end);
							size_t size = fs.tellg();
							fs.seekg(0, std::ios::beg);

							if(size > 0)
							{
								m_fileContents.resize(size);
								if(fs.read(m_fileContents.data(), size))
								{
									m_fileContentsValid = true;
									notify();
								}
							}
						}
						else
						{
							m_message = "Cannot read file contents! (" + m_filePath + m_fileName + ")";
							m_fileContentsValid = false;
							notify();
						}
					}
					break;
				}
				default:
				{
					// other changes we don't care about
					break;
				}
				}

				// Handle remaining events (if any)
				if(event->NextEntryOffset)
				{
					*((uint8_t**)&event) += event->NextEntryOffset;
				}
				else
				{
					break;
				}
			}
			break;
		}
		case WAIT_TIMEOUT:
		{
			// nothing happened within period, repeat
			break;
		}
		}

		if(stopSig.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
		{
			break;
		}
	}

	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopping <" << m_fileName << ">";
}

}
