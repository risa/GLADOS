// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <glados/Utils.h>
#include <glados/observer/FileObserver.h>

#include <fstream>
#include <poll.h>
#include <sys/stat.h>
#include <unistd.h>

namespace glados
{
auto FileObserver::attach(glados::Observer* o) -> void { m_observers.push_back(o); }

auto FileObserver::detach(glados::Observer* o) -> void { m_observers.remove(o); }

auto FileObserver::notify() -> void
{
	for(auto& o : m_observers)
		o->update(this);
}

auto FileObserver::setFilePathToObserve(std::string filePath, std::string fileName) -> bool
{

	if(filePath.back() != '/')
		filePath.push_back('/');

	glados::utils::FileSystemHandle fsh;

	try
	{

		if(!fsh.createDirectory(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access path to observe '" << filePath
									 << "'.";
			return false;
		}

		// check if file to observe exists
		std::string fullPath = filePath + fileName;
		if(!fsh.createEmptyFile(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Cannot access file to observe '" << fullPath
									 << "'.";
			return false;
		}

		if(filePath != "/tmp/" && !fsh.setFilePermissionsWriteAll(filePath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration directory '"
				<< filePath << "'.";
			return false;
		}

		if(!fsh.setFilePermissionsWriteAll(fullPath))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "glados::FileObserver:: Cannot grant write access to runtime configuration file'"
				<< fullPath << "'.";
			return false;
		}

		// initialize inotify
		m_inotifyInstance = inotify_init();
		if(m_inotifyInstance < 0)
		{
			// something wrong with inotify
			return false;
		}
	}
	catch(std::runtime_error const& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Error when initializing FileObserver: "
								 << e.what();
		return false;
	}
	catch(...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "glados::FileObserver:: Unknown error when initializing FileObserver.";
		return false;
	}

	m_fileName = fileName;
	m_filePath = filePath;
	m_fileContentsValid = false;
	return true;
}

auto FileObserver::setEventsToObserve(uint32_t inotifyEvents) -> void { m_observedEvents = inotifyEvents; }

auto FileObserver::start() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Starting to observe \"" << m_filePath << m_fileName
							 << "\"";
	std::future<void> stopSigFuture = m_stopSig.get_future();
	m_observerThread = std::thread{&FileObserver::observe, this, std::move(stopSigFuture)};
}

auto FileObserver::wait() -> void
{
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Requesting to stop...";
	m_stopSig.set_value();
	m_observerThread.join();
	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopped.";
}

auto FileObserver::observe(std::future<void> stopSig) -> void
{
	m_watchDescriptor = inotify_add_watch(m_inotifyInstance, m_filePath.c_str(), m_observedEvents);

	char buffer[m_EVENT_BUF_LEN];
	ssize_t i = 0, length;

	while(true)
	{
		struct pollfd pfd = {m_inotifyInstance, POLLIN, 0};
		int ret =
			poll(&pfd, 1, 50); // poll for file changes every 50 ms -> allows us to break in between polls

		if(ret > 0)
		{
			length = read(m_inotifyInstance, buffer, m_EVENT_BUF_LEN);

			if(length < 0)
			{
				BOOST_LOG_TRIVIAL(error) << "glados::FileObserver:: Error when reading inotify instance.";
				return;
			}
			i = 0;
			while(i < length)
			{
				struct inotify_event* event = (struct inotify_event*)&buffer[i];
				if(event->len)
				{
					if(event->mask & m_observedEvents)
					{
						if(!(event->mask & IN_ISDIR))
						{
							std::string changedFile = event->name;
							if(changedFile == m_fileName)
							{
								m_message = "File changed!";
								std::ifstream fs(m_filePath + m_fileName, std::ios::binary | std::ios::ate);
								std::streamsize size = fs.tellg();
								fs.seekg(0, std::ios::beg);
								m_fileContents.resize(size);
								if(size > 0 && fs.read(m_fileContents.data(), size))
								{
									m_fileContentsValid = true;
									notify();
								}
								else
								{
									m_message = "Cannot read file contents!";
									m_fileContentsValid = false;
									notify();
								}
							}
						}
					}
				}
				i += m_EVENT_SIZE + event->len;
			}
		}

		if(stopSig.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
		{
			break;
		}
	}

	BOOST_LOG_TRIVIAL(debug) << "glados::FileObserver:: Stopping <" << m_fileName << ">";
	inotify_rm_watch(m_inotifyInstance, m_watchDescriptor);
	close(m_inotifyInstance);
}

}
