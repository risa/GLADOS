// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// Andr� Bieberle (a.bieberle@hzdr.de)

#include <array>
#include <cstdio>
#include <glados/Utils.h>
#include <memory>
#include <string>

#ifdef _WIN32
#include <windows.h>
namespace glados::utils
{

const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push, 8)
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void setThreadName(const char* threadName)
{
	uint32_t dwThreadID = GetCurrentThreadId();
	// DWORD dwThreadID = ::GetThreadId( static_cast<HANDLE>( t.native_handle() ) );

	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = threadName;
	info.dwThreadID = dwThreadID;
	info.dwFlags = 0;

	__try
	{
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
	}
}
}

#elif defined(__linux__)
#include <sys/prctl.h>
namespace glados::utils
{
void setThreadName(const char* threadName) { prctl(PR_SET_NAME, threadName, 0, 0, 0); }
}
#endif

namespace glados::utils
{
#ifdef _WIN32
#define pclose _pclose
#define popen _popen
#endif
auto ShellHandle::exec(const char* cmd) -> std::string
{
	constexpr const size_t bufferSize{128};
	std::array<char, bufferSize> buffer{};
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if(!pipe)
	{
		throw std::runtime_error("popen() failed!");
	}
	while(fgets(buffer.data(), (int)buffer.size(), pipe.get()) != nullptr)
	{
		result += buffer.data();
	}
	if(result.back() == '\n')
	{
		result.pop_back();
	}
	return result;
}

auto FileSystemHandle::createDirectory(std::string path) -> bool
{
	try
	{
		if(path.back() == '/' || path.back() == '\\')
		{
			path.pop_back(); // else std::filesystem will return false because it cannot create an unnamed
							 // subdirectory
		}
		auto fspath = std::filesystem::path{path};
		if(std::filesystem::exists(fspath))
		{
			if(std::filesystem::is_directory(fspath))
			{
				return true;
			}
			throw std::runtime_error(path + " exists but is not a directory.");
		}
		return std::filesystem::create_directories(fspath);
	}
	catch(const std::filesystem::filesystem_error& err)
	{
		BOOST_LOG_TRIVIAL(fatal) << path << " could not be created: " << err.what();
		return false;
	}
}

auto FileSystemHandle::createEmptyFile(const std::string& path) -> bool
{
	try
	{
		auto fs_path = std::filesystem::path{path};
		if(std::filesystem::exists(fs_path))
		{
			if(std::filesystem::is_directory(fs_path))
			{
				throw std::runtime_error(path + " exists but is a directory.");
			}
			return true;
		}
		auto ofs = std::ofstream(path);
		return ofs.good();
	}
	catch(const std::filesystem::filesystem_error& err)
	{
		BOOST_LOG_TRIVIAL(fatal) << path << " could not be created: " << err.what();
		return false;
	}
}

auto FileSystemHandle::setFilePermissionsWriteAll(const std::string& path) -> bool
{
	try
	{
		auto fs_path = std::filesystem::path{path};
		if(std::filesystem::exists(fs_path))
		{
			std::filesystem::permissions(fs_path,
				std::filesystem::perms::owner_write | std::filesystem::perms::group_write
					| std::filesystem::perms::others_write,
				std::filesystem::perm_options::add);
			return true;
		}
		throw std::runtime_error(path + " does not exist.");
	}
	catch(const std::filesystem::filesystem_error& err)
	{
		BOOST_LOG_TRIVIAL(fatal) << path << " could not be modified: " << err.what();
		return false;
	}
}

auto FileSystemHandle::setFilePermissionsAllAll(const std::string& path) -> bool
{
	try
	{
		auto fs_path = std::filesystem::path{path};
		if(std::filesystem::exists(fs_path))
		{
			std::filesystem::permissions(
				fs_path, std::filesystem::perms::all, std::filesystem::perm_options::add);
			return true;
		}
		throw std::runtime_error(path + " does not exist.");
	}
	catch(const std::filesystem::filesystem_error& err)
	{
		BOOST_LOG_TRIVIAL(fatal) << path << " could not be modified: " << err.what();
		return false;
	}
}

} // namespace glados::utils
