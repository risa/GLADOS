// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <algorithm>
#include <cstdlib>

#define BOOST_TEST_MODULE CudaCopy
#include <boost/test/unit_test.hpp>

#include <glados/cuda/algorithm.h>
#include <glados/cuda/memory.h>
#include <glados/cuda/sync_policy.h>

BOOST_AUTO_TEST_CASE(cuda_copy_sync_1d)
{
    constexpr auto szx = 4096;
    constexpr auto dim = szx;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx);
    auto dev = glados::cuda::make_unique_device<int>(szx);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::sync, dev, host_orig, szx);
    glados::cuda::copy(glados::cuda::sync, host_dest, dev, szx);

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_copy_sync_2d)
{
    constexpr auto szx = 64;
    constexpr auto szy = 64;
    constexpr auto dim = szx * szy;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto dev = glados::cuda::make_unique_device<int>(szx, szy);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::sync, dev, host_orig, szx, szy);
    glados::cuda::copy(glados::cuda::sync, host_dest, dev, szx, szy);

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_copy_sync_3d)
{
    constexpr auto szx = 8;
    constexpr auto szy = 8;
    constexpr auto szz = 8;
    constexpr auto dim = szx * szy * szz;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto dev = glados::cuda::make_unique_device<int>(szx, szy, szz);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::sync, dev, host_orig, szx, szy, szz);
    glados::cuda::copy(glados::cuda::sync, host_dest, dev, szx, szy, szz);

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_copy_async_1d)
{
    constexpr auto szx = 4096;
    constexpr auto dim = szx;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx);
    auto dev = glados::cuda::make_unique_device<int>(szx);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::async, dev, host_orig, szx);
    cudaDeviceSynchronize();
    glados::cuda::copy(glados::cuda::async, host_dest, dev, szx);
    cudaDeviceSynchronize();

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_copy_async_2d)
{
    constexpr auto szx = 64;
    constexpr auto szy = 64;
    constexpr auto dim = szx * szy;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto dev = glados::cuda::make_unique_device<int>(szx, szy);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::async, dev, host_orig, szx, szy);
    cudaDeviceSynchronize();
    glados::cuda::copy(glados::cuda::async, host_dest, dev, szx, szy);
    cudaDeviceSynchronize();

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_copy_async_3d)
{
    constexpr auto szx = 8;
    constexpr auto szy = 8;
    constexpr auto szz = 8;
    constexpr auto dim = szx * szy * szz;

    auto host_orig = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto host_dest = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto dev = glados::cuda::make_unique_device<int>(szx, szy, szz);

    auto ho = host_orig.get();
    auto hd = host_dest.get();

    std::generate(ho, ho + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::async, dev, host_orig, szx, szy, szz);
    cudaDeviceSynchronize();
    glados::cuda::copy(glados::cuda::async, host_dest, dev, szx, szy, szz);
    cudaDeviceSynchronize();

    BOOST_CHECK(std::equal(ho, ho + dim, hd));
}
