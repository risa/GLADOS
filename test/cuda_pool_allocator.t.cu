// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <algorithm>
#include <cstdlib>

#define BOOST_TEST_MODULE CudaPoolAllocator
#include <boost/test/unit_test.hpp>

#include <glados/cuda/algorithm.h>
#include <glados/cuda/memory.h>
#include <glados/cuda/sync_policy.h>
#include <glados/memory.h>

BOOST_AUTO_TEST_CASE(cuda_pool_alloc_1d)
{
    constexpr auto szx = 4096;
    constexpr auto dim = szx;

    using device_allocator_type = glados::cuda::device_allocator<int, glados::memory_layout::pointer_1D>;
    using pool_allocator_type = glados::pool_allocator<int, glados::memory_layout::pointer_1D, device_allocator_type>;
    auto alloc = pool_allocator_type{};

    auto host_src = glados::cuda::make_unique_pinned_host<int>(szx);
    auto host_dst = glados::cuda::make_unique_pinned_host<int>(szx);
    auto dev = alloc.allocate_smart(szx);

    auto hs = host_src.get();
    auto hd = host_dst.get();

    std::generate(hs, hs + dim, std::rand);
    std::fill(hd, hd + dim, 0);
    glados::cuda::fill(glados::cuda::sync, dev, 0, szx);

    glados::cuda::copy(glados::cuda::sync, dev, host_src, szx);
    glados::cuda::copy(glados::cuda::sync, host_dst, dev, szx);

    BOOST_CHECK(std::equal(hs, hs + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_pool_alloc_2d)
{
    constexpr auto szx = 64;
    constexpr auto szy = 64;
    constexpr auto dim = szx * szy;

    using device_allocator_type = glados::cuda::device_allocator<int, glados::memory_layout::pointer_2D>;
    using pool_allocator_type = glados::pool_allocator<int, glados::memory_layout::pointer_2D, device_allocator_type>;
    auto alloc = pool_allocator_type{};

    auto host_src = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto host_dst = glados::cuda::make_unique_pinned_host<int>(szx, szy);
    auto dev = alloc.allocate_smart(szx, szy);
    glados::cuda::fill(glados::cuda::sync, dev, 0, szx, szy);

    auto hs = host_src.get();
    auto hd = host_dst.get();

    std::generate(hs, hs + dim, std::rand);
    std::fill(hd, hd + dim, 0);

    glados::cuda::copy(glados::cuda::sync, dev, host_src, szx, szy);
    glados::cuda::copy(glados::cuda::sync, host_dst, dev, szx, szy);

    BOOST_CHECK(std::equal(hs, hs + dim, hd));
}

BOOST_AUTO_TEST_CASE(cuda_pool_alloc_3d)
{
    constexpr auto szx = 8;
    constexpr auto szy = 8;
    constexpr auto szz = 8;
    constexpr auto dim = szx * szy * szz;

    using device_allocator_type = glados::cuda::device_allocator<int, glados::memory_layout::pointer_3D>;
    using pool_allocator_type = glados::pool_allocator<int, glados::memory_layout::pointer_3D, device_allocator_type>;
    auto alloc = pool_allocator_type{};

    auto host_src = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto host_dst = glados::cuda::make_unique_pinned_host<int>(szx, szy, szz);
    auto dev = alloc.allocate_smart(szx, szy, szz);

    auto hs = host_src.get();
    auto hd = host_dst.get();

    std::generate(hs, hs + dim, std::rand);
    std::fill(hd, hd + dim, 0);
    glados::cuda::fill(glados::cuda::sync, dev, 0, szx, szy, szz);

    glados::cuda::copy(glados::cuda::sync, dev, host_src, szx, szy, szz);
    glados::cuda::copy(glados::cuda::sync, host_dst, dev, szx, szy, szz);

    BOOST_CHECK(std::equal(hs, hs + dim, hd));
}
