// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <thread>

#define BOOST_TEST_MODULE CudaUninitialized
#include <boost/test/unit_test.hpp>

#include <glados/cuda/algorithm.h>
#include <glados/cuda/coordinates.h>
#include <glados/cuda/launch.h>
#include <glados/cuda/memory.h>
#include <glados/cuda/sync_policy.h>
#include <glados/memory.h>

__global__ void kernel(float *dst, std::size_t d_p, const float* src, std::size_t s_p, std::size_t w, std::size_t h)
{
    auto x = glados::cuda::coord_x();
    auto y = glados::cuda::coord_y();

    if((x < w) && (y < h))
    {
        auto src_row = reinterpret_cast<const float*>(reinterpret_cast<const char*>(src) + s_p * y);
        auto dst_row = reinterpret_cast<float*>(reinterpret_cast<char*>(dst) + d_p * y);

        dst_row[x] = src_row[x] + 42;
    }
}


BOOST_AUTO_TEST_CASE(cuda_uninitialized_make_unique)
{
    constexpr auto w = std::size_t{1024};
    constexpr auto h = std::size_t{768};

    auto src = glados::cuda::make_unique_device<float>(w, h);
    auto dst = glados::cuda::make_unique_device<float>(w, h);

    glados::cuda::fill(glados::cuda::sync, src, 0x01, w, h);
    glados::cuda::launch(w, h, kernel, dst.get(), dst.pitch(), static_cast<const float*>(src.get()), src.pitch(), w, h);
    cudaDeviceSynchronize();
}

BOOST_AUTO_TEST_CASE(cuda_uninitialized_pool)
{
    constexpr auto w = std::size_t{1024};
    constexpr auto h = std::size_t{768};

    using device_allocator = glados::cuda::device_allocator<float, glados::memory_layout::pointer_2D>;
    using pool_allocator = glados::pool_allocator<float, glados::memory_layout::pointer_2D, device_allocator>;
    auto alloc = pool_allocator{};

    auto src = alloc.allocate_smart(w, h);
    auto dst = alloc.allocate_smart(w, h);

    glados::cuda::launch(w, h, kernel, dst.get(), dst.pitch(), static_cast<const float*>(src.get()), src.pitch(), w, h);
    cudaDeviceSynchronize();
}

BOOST_AUTO_TEST_CASE(cuda_uninitialized_make_unique_stream)
{
    constexpr auto w = std::size_t{1024};
    constexpr auto h = std::size_t{768};

    auto t = std::thread{[w, h]() {
            auto src = glados::cuda::make_unique_device<float>(w, h);
            auto dst = glados::cuda::make_unique_device<float>(w, h);

            glados::cuda::fill(glados::cuda::sync, src, 0x01, w, h);
            glados::cuda::launch(w, h, kernel, dst.get(), dst.pitch(), static_cast<const float*>(src.get()), src.pitch(), w, h);
            cudaDeviceSynchronize();
        }
    };
    t.join();
}

BOOST_AUTO_TEST_CASE(cuda_uninitialized_pool_stream)
{
    constexpr auto w = std::size_t{1024};
    constexpr auto h = std::size_t{768};

    using device_allocator = glados::cuda::device_allocator<float, glados::memory_layout::pointer_2D>;
    using pool_allocator = glados::pool_allocator<float, glados::memory_layout::pointer_2D, device_allocator>;
    auto t = std::thread{[w, h]() {
            auto alloc = pool_allocator{};

            auto src = alloc.allocate_smart(w, h);
            auto dst = alloc.allocate_smart(w, h);

            glados::cuda::launch(w, h, kernel, dst.get(), dst.pitch(), static_cast<const float*>(src.get()), src.pitch(), w, h);
            cudaDeviceSynchronize();
        }
    };
    t.join();
}
