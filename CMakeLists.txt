# This CMAKE file is speficially for use as submodule in the RISA repository
cmake_minimum_required(VERSION 3.5)
include(CheckIncludeFileCXX)
include_directories(
   ${Boost_INCLUDE_DIRS}
   "${CMAKE_SOURCE_DIR}/glados/include"
)

set(SOURCES
   "${CMAKE_SOURCE_DIR}/glados/src/observer/Subject.cpp"
   "${CMAKE_SOURCE_DIR}/glados/src/Utils.cpp"
)

if(UNIX)
	set(SOURCES ${SOURCES}
		"${CMAKE_SOURCE_DIR}/glados/src/observer/FileObserver_linux.cpp"
	)
endif(UNIX)

if(WIN32)
	set(SOURCES ${SOURCES}
		"${CMAKE_SOURCE_DIR}/glados/src/observer/FileObserver_win32.cpp"
	)
endif(WIN32)

set(LINK_LIBRARIES ${LINK_LIBRARIES}
   ${Boost_LIBRARIES} 
)
link_directories(${Boost_LIBRARY_DIRS})

set(CMAKE_REQUIRED_INCLUDES ${CUDA_INCLUDE_DIRS})
CHECK_INCLUDE_FILE_CXX(nvjpeg.h NVJPEG_AVAILABLE)

if(NVJPEG_AVAILABLE)
	add_definitions(-DNVJPEG_AVAILABLE)
endif(NVJPEG_AVAILABLE)

add_library(glados ${SOURCES})
add_library(singleton SHARED "${CMAKE_SOURCE_DIR}/glados/src/Singleton.cpp")

target_link_libraries(glados singleton ${LINK_LIBRARIES})
