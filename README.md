# GLADOS - Generic Library for Asynchronous Data Operations and Streaming

## Build instructions

Header only, no need to build it.

### Instructions can be found in the [Wiki](https://gitlab.hzdr.de/risa/GLADOS/-/wikis/home)
