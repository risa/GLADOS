// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUFFT_PLAN_H_
#define GLADOS_CUFFT_PLAN_H_

#include <cstddef>
#include <type_traits>
#include <utility>

#include <cufft.h>

#include <glados/cufft/exception.h>

namespace glados
{
    namespace cufft
    {
        namespace detail
        {
            template <class I, class O> struct type_chooser {};
            template <> struct type_chooser<cufftReal, cufftComplex> { static constexpr auto value = CUFFT_R2C; };
            template <> struct type_chooser<cufftComplex, cufftReal> { static constexpr auto value = CUFFT_C2R; };
            template <> struct type_chooser<cufftComplex, cufftComplex> { static constexpr auto value = CUFFT_C2C; };
            template <> struct type_chooser<cufftDoubleReal, cufftDoubleComplex> { static constexpr auto value = CUFFT_D2Z; };
            template <> struct type_chooser<cufftDoubleComplex, cufftDoubleReal> { static constexpr auto value = CUFFT_Z2D; };
            template <> struct type_chooser<cufftDoubleComplex, cufftDoubleComplex> { static constexpr auto value = CUFFT_Z2Z; };

            template <class I> struct type_mapper {};
            template <> struct type_mapper<cufftReal> { using type = cufftComplex; };
            template <> struct type_mapper<cufftComplex> { using type = cufftReal; };
            template <> struct type_mapper<cufftDoubleReal> { using type = cufftDoubleComplex; };
            template <> struct type_mapper<cufftDoubleComplex> { using type = cufftDoubleReal; };
        }

        template <cufftType type>
        class plan
        {
            public:
                static constexpr auto transformation_type = type;

                plan() noexcept = default;
                plan(int nx) : m_valid{true} { handle_result(cufftPlan1d(&m_handle, nx, transformation_type, 1)); }
                plan(int nx, int ny) : m_valid{true} { handle_result(cufftPlan2d(&m_handle, nx, ny, transformation_type)); }
                plan(int nx, int ny, int nz) : m_valid{true} { handle_result(cufftPlan3d(&m_handle, nx, ny, nz, transformation_type)); }

                plan(int rank, int* n, int* inembed, int istride, int idist,
                                       int* onembed, int ostride, int odist,
                                       int batch)
                : m_valid{true}
                {
                    handle_result(cufftPlanMany(&m_handle, rank, n, inembed, istride, idist, onembed, ostride, odist, transformation_type, batch));
                }

                plan(const plan& other) noexcept
                : m_handle{other.m_handle}, m_valid{other.m_valid}
                {}

                auto operator=(const plan& other) noexcept -> plan&
                {
                    m_handle = other.m_handle;
                    m_valid = other.m_valid;
                    return *this;
                }


                plan(plan&& other) noexcept
                : m_handle{std::move(other.m_handle)}, m_valid{other.m_valid}
                {
                    other.m_valid = false;
                }

                auto operator=(plan&& other) noexcept -> plan&
                {
                    m_handle = std::move(other.m_handle);
                    m_valid = other.m_valid;
                    other.m_valid = false;
                    return *this;
                }

                ~plan() { if(m_valid) cufftDestroy(m_handle); }

                auto get() noexcept -> cufftHandle
                {
                    return m_handle;
                }

                template <class I, class O>
                auto execute(I* idata, O* odata) -> void
                {
                    static_assert(!std::is_same<I, O>::value, "Plan needs a direction for transformations between the same types.");
                    static_assert(detail::type_chooser<I, O>::value == transformation_type, "This plan can not be used for other types than originally specified.");
                    static_assert(std::is_same<O, typename detail::type_mapper<I>::type>::value, "Attempt to transform to an incompatible type.");

                    cufft_exec(idata, odata);
                }

                template <class I, class O>
                auto execute(I* idata, O* odata, int direction) -> void
                {
                    static_assert(std::is_same<I, O>::value, "Transformations between different types are implicitly inverse");
                    static_assert(detail::type_chooser<I, O>::value == transformation_type, "This plan can not be used for other types than originally specified.");
                    static_assert(std::is_same<O, typename detail::type_mapper<I>::type>::value, "Attempt to transform to an incompatible type.");

                    cufft_exec(idata, odata, direction);
                }

                auto set_stream(cudaStream_t stream) -> void
                {
                    handle_result(cufftSetStream(m_handle, stream));
                }

            private:
                auto handle_result(cufftResult res) const -> void
                {
                    #pragma GCC diagnostic push
                    #pragma GCC diagnostic ignored "-Wswitch-enum"
                    switch(res)
                    {
                        case CUFFT_SUCCESS:         break;
                        case CUFFT_INVALID_PLAN:    throw invalid_argument{"The plan parameter is not a valid handle."};
                        case CUFFT_ALLOC_FAILED:    throw bad_alloc{};
                        case CUFFT_INVALID_VALUE:   throw invalid_argument{"One or more invalid parameters were passed to the API."};
                        case CUFFT_INTERNAL_ERROR:  throw runtime_error{"An internal driver error was detected."};
                        case CUFFT_EXEC_FAILED:     throw runtime_error{"cuFFT failed to execute the transform on the GPU."};
                        case CUFFT_SETUP_FAILED:    throw runtime_error{"The cuFFT library failed to initialize."};
                        case CUFFT_INVALID_SIZE:    throw invalid_argument{"One or more of the parameters is not a supported size."};
                        default:                    throw runtime_error{"Unknown error."};
                    }
                    #pragma GCC diagnostic pop
                }

                auto cufft_exec(cufftComplex* idata, cufftComplex* odata, int direction) -> void
                {
                    handle_result(cufftExecC2C(m_handle, idata, odata, direction));
                }

                auto cufft_exec(cufftDoubleComplex* idata, cufftDoubleComplex* odata, int direction) -> void
                {
                    handle_result(cufftExecZ2Z(m_handle, idata, odata, direction));
                }

                auto cufft_exec(cufftReal* idata, cufftComplex* odata) -> void
                {
                    handle_result(cufftExecR2C(m_handle, idata, odata));
                }

                auto cufft_exec(cufftDoubleReal* idata, cufftDoubleComplex* odata) -> void
                {
                    handle_result(cufftExecD2Z(m_handle, idata, odata));
                }

                auto cufft_exec(cufftComplex* idata, cufftReal* odata) -> void
                {
                    handle_result(cufftExecC2R(m_handle, idata, odata));
                }

                auto cufft_exec(cufftDoubleComplex* idata, cufftDoubleReal* odata) -> void
                {
                    handle_result(cufftExecZ2D(m_handle, idata, odata));
                }

            private:
                bool m_valid = false;
                cufftHandle m_handle = 0;
        };
    }
}

#endif /* GLADOS_CUFFT_PLAN_H_ */
