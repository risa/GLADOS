// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_BITS_POOL_ALLOCATOR_H_
#define GLADOS_BITS_POOL_ALLOCATOR_H_

#include <atomic>
#include <forward_list>
#include <functional>
#include <thread>
#include <type_traits>
#include <utility>

#include <glados/bits/memory_layout.h>

namespace glados
{
    template <class T, memory_layout ml, class InternalAlloc, class = typename std::enable_if<(ml == InternalAlloc::mem_layout)>::type>
    class pool_allocator {};

    /* 1D specialization */
    template <class T, class InternalAlloc>
    class pool_allocator<T, memory_layout::pointer_1D, InternalAlloc>
    {
        public:
            static constexpr auto mem_layout = InternalAlloc::mem_layout;
            static constexpr auto mem_location = InternalAlloc::mem_location;
            static constexpr auto alloc_needs_pitch = InternalAlloc::alloc_needs_pitch;

            using value_type = T;
            using pointer = typename InternalAlloc::pointer;
            using const_pointer = typename InternalAlloc::const_pointer;
            using size_type = typename InternalAlloc::size_type;
            using difference_type = typename InternalAlloc::difference_type;
            using propagate_on_container_copy_assignment = std::true_type;
            using propagate_on_container_move_assignment = std::true_type;
            using propagate_on_container_swap = std::true_type;
            using is_always_equal = std::true_type;
            using smart_pointer = typename InternalAlloc::template smart_pointer<std::function<void(T*)>>;

            template <class U>
            struct rebind
            {
                using other = pool_allocator<U, mem_layout, InternalAlloc>;
            };

        public:
            pool_allocator() = default;

            pool_allocator(size_type limit) noexcept
            : m_alloc{}, m_list{}, n_{}, m_limit{limit}, m_current{0}
            {}

            pool_allocator(pool_allocator&& other) noexcept
            : m_alloc{std::move(other.m_alloc)}, m_list{std::move(other.m_list)}
            , n_{other.n_}, m_limit{other.m_limit}, m_current{other.m_current.load()}, m_moved{other.m_moved}
            {
                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;
            }

            auto operator=(pool_allocator&& other) noexcept -> pool_allocator&
            {
                m_alloc = std::move(other.m_alloc);
                m_list = std::move(other.m_list);
                n_ = other.n_;
                m_limit = other.m_limit;
                m_current = other.m_current.load();
                m_moved = other.m_moved;

                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;

                return *this;
            }

            pool_allocator(const pool_allocator& other) noexcept = delete;
            auto operator=(const pool_allocator& other) noexcept -> pool_allocator& = delete;

            ~pool_allocator()
            {
                // pool_allocator's contents have to be released manually
            }

            auto allocate(size_type n) -> pointer
            {
                if(m_moved)
                    return pointer{nullptr};

                if(n_ == 0)
                    n_ = n;

                auto ret = static_cast<pointer>(nullptr);

                if(m_limit != 0)
                {
                    while(m_current.load() >= m_limit)
                        std::this_thread::yield();
                }

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                if(m_list.empty())
                    ret = m_alloc.allocate(n_);
                else
                {
                    ret = std::move(m_list.front());
                    m_list.pop_front();
                }

                ++m_current;
                m_lock.clear(std::memory_order_release);

                return ret;
            }

            auto allocate_smart(size_type n) -> smart_pointer
            {
                return smart_pointer{allocate(n), [this](T* ptr){ this->deallocate(ptr); }};
            }

            auto deallocate(pointer p, size_type = 0) noexcept -> void
            {
                if(m_moved || (p == nullptr))
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                m_list.push_front(p);
                --m_current;
                m_lock.clear(std::memory_order_release);
            }

            auto release() noexcept -> void
            {
                if(m_moved)
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                for(auto&& p : m_list)
                    m_alloc.deallocate(p, n_);

                n_ = 0;
                m_current.store(0);

                m_lock.clear(std::memory_order_release);
            }

        private:
            InternalAlloc m_alloc;
            std::atomic_flag m_lock = ATOMIC_FLAG_INIT;
            std::forward_list<pointer> m_list;
            size_type n_;
            size_type m_limit;
            std::atomic_size_t m_current;
            bool m_moved = false;
    };

    /* 2D specialization */
    template <class T, class InternalAlloc>
    class pool_allocator<T, memory_layout::pointer_2D, InternalAlloc>
    {
        public:
            static constexpr auto mem_layout = InternalAlloc::mem_layout;
            static constexpr auto mem_location = InternalAlloc::mem_location;
            static constexpr auto alloc_needs_pitch = InternalAlloc::alloc_needs_pitch;

            using value_type = T;
            using pointer = typename InternalAlloc::pointer;
            using const_pointer = typename InternalAlloc::const_pointer;
            using size_type = typename InternalAlloc::size_type;
            using difference_type = typename InternalAlloc::difference_type;
            using propagate_on_container_copy_assignment = std::true_type;
            using propagate_on_container_move_assignment = std::true_type;
            using propagate_on_container_swap = std::true_type;
            using is_always_equal = std::true_type;
            using smart_pointer = typename InternalAlloc::template smart_pointer<std::function<void(T*)>>;

            template <class U>
            struct rebind
            {
                using other = pool_allocator<U, mem_layout, InternalAlloc>;
            };

        public:
            pool_allocator() = default;

            pool_allocator(size_type limit)
            : m_alloc{}, m_list{}, x_{0}, y_{0}, m_limit{limit}, m_current{0}
            {}

            pool_allocator(pool_allocator&& other) noexcept
            : m_alloc{std::move(other.m_alloc)}, m_list{std::move(other.m_list)}
            , x_{other.x_}, y_{other.y_}, m_limit{other.m_limit}, m_current{other.m_current.load()}, m_moved{other.m_moved}
            {
                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;
            }

            auto operator=(pool_allocator&& other) noexcept -> pool_allocator&
            {
                m_alloc = std::move(other.m_alloc);
                m_list = std::move(other.m_list);
                x_ = other.x_;
                y_ = other.y_;
                m_limit = other.m_limit;
                m_current = other.m_current.load();
                m_moved = other.m_moved;
                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;

                return *this;
            }

            pool_allocator(const pool_allocator& other) noexcept = delete;
            auto operator=(const pool_allocator& other) noexcept -> pool_allocator& = delete;

            ~pool_allocator()
            {
                // pool_allocator's contents have to be released manually
            }

            auto allocate(size_type x, size_type y) -> pointer
            {
                if(m_moved)
                    return pointer{nullptr};

                if(x_ == 0)
                    x_ = x;

                if(y_ == 0)
                    y_ = y;

                auto ret = static_cast<pointer>(nullptr);

                if(m_limit != 0)
                {
                    while(m_current.load() >= m_limit)
                        std::this_thread::yield();
                }

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                if(m_list.empty())
                    ret = m_alloc.allocate(x_, y_);
                else
                {
                    ret = std::move(m_list.front());
                    m_list.pop_front();
                }

                ++m_current;
                m_lock.clear(std::memory_order_release);

                return ret;
            }

            auto allocate_smart(size_type x, size_type y) -> smart_pointer
            {
                auto p = allocate(x, y);
                return smart_pointer{p, [this, p](T*){ this->deallocate(p); }};
            }

            auto deallocate(pointer p, size_type = 0, size_type = 0) noexcept -> void
            {
                if(m_moved || (p == nullptr))
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                m_list.push_front(p);
                --m_current;
                m_lock.clear(std::memory_order_release);
            }

            auto release() noexcept -> void
            {
                if(m_moved) // the allocator becomes invalid once moved from
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                for(auto&& p : m_list)
                    m_alloc.deallocate(p, x_, y_);

                x_ = 0;
                y_ = 0;
                m_current.store(0);

                m_lock.clear(std::memory_order_release);
            }

        private:
            InternalAlloc m_alloc;
            std::atomic_flag m_lock = ATOMIC_FLAG_INIT;
            std::forward_list<pointer> m_list;
            size_type x_;
            size_type y_;
            size_type m_limit;
            std::atomic_size_t m_current;
            bool m_moved = false;
    };

    /* 3D specialization */
    template <class T, class InternalAlloc>
    class pool_allocator<T, memory_layout::pointer_3D, InternalAlloc>
    {
        public:
            static constexpr auto mem_layout = InternalAlloc::mem_layout;
            static constexpr auto mem_location = InternalAlloc::mem_location;
            static constexpr auto alloc_needs_pitch = InternalAlloc::alloc_needs_pitch;
            using propagate_on_container_copy_assignment = std::true_type;
            using propagate_on_container_move_assignment = std::true_type;
            using propagate_on_container_swap = std::true_type;
            using is_always_equal = std::true_type;

            using value_type = T;
            using pointer = typename InternalAlloc::pointer;
            using const_pointer = typename InternalAlloc::const_pointer;
            using size_type = typename InternalAlloc::size_type;
            using difference_type = typename InternalAlloc::difference_type;
            using smart_pointer = typename InternalAlloc::template smart_pointer<std::function<void(T*)>>;

            template <class U>
            struct rebind
            {
                using other = pool_allocator<U, mem_layout, InternalAlloc>;
            };

        public:
            pool_allocator() = default;

            pool_allocator(size_type limit)
            : m_alloc{}, m_list{}, x_{}, y_{}, z_{}, m_limit{limit}, m_current{0}
            {}

            pool_allocator(pool_allocator&& other) noexcept
            : m_alloc{std::move(other.m_alloc)}, m_list{std::move(other.m_list)}
            , x_{other.x_}, y_{other.y_}, z_{other.z_}, m_limit{other.m_limit}, m_current{other.m_current.load()}, m_moved{other.m_moved}
            {
                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;
            }

            auto operator=(pool_allocator&& other) noexcept -> pool_allocator&
            {
                m_alloc = std::move(other.m_alloc);
                m_list = std::move(other.m_list);
                x_ = other.x_;
                y_ = other.y_;
                z_ = other.z_;
                m_limit = other.m_limit;
                m_current = other.m_current.load();
                m_moved = other.m_moved;
                if(other.m_lock.test_and_set())
                    m_lock.test_and_set();
                else
                    m_lock.clear();

                other.m_moved = true;

                return *this;
            }

            pool_allocator(const pool_allocator& other) noexcept = delete;
            auto operator=(const pool_allocator& other) noexcept -> pool_allocator& = delete;

            ~pool_allocator()
            {
                // pool_allocator's contents have to be released manually
            }

            auto allocate(size_type x, size_type y, size_type z) -> pointer
            {
                if(m_moved)
                    return pointer{nullptr};

                if(x_ == 0)
                    x_ = x;
                if(y_ == 0)
                    y_ = y;
                if(z_ == 0)
                    z_ = z;

                if(m_limit != 0)
                {
                    while(m_current.load() >= m_limit)
                        std::this_thread::yield();
                }

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                auto ret = static_cast<pointer>(nullptr);

                if(m_list.empty())
                    ret = m_alloc.allocate(x_, y_, z_);
                else
                {
                    ret = std::move(m_list.front());
                    m_list.pop_front();
                }

                ++m_current;
                m_lock.clear(std::memory_order_release);

                return ret;
            }

            auto allocate_smart(size_type x, size_type y, size_type z) -> smart_pointer
            {
                auto p = allocate(x, y, z);
                return smart_pointer{p, [this, p](T*){ this->deallocate(p); }};
            }

            auto deallocate(pointer p, size_type = 0, size_type = 0, size_type = 0) noexcept -> void
            {
                if(m_moved || (p == nullptr))
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                m_list.push_front(p);
                --m_current;
                m_lock.clear(std::memory_order_release);
            }

            auto release() noexcept -> void
            {
                if(m_moved) // the allocator becomes invalid once moved from
                    return;

                while(m_lock.test_and_set(std::memory_order_acquire))
                    std::this_thread::yield();

                for(auto&& p : m_list)
                    m_alloc.deallocate(p, x_, y_, z_);

                x_ = 0;
                y_ = 0;
                z_ = 0;
                m_current.store(0);

                m_lock.clear(std::memory_order_release);
            }

        private:
            InternalAlloc m_alloc;
            std::atomic_flag m_lock = ATOMIC_FLAG_INIT;
            std::forward_list<pointer> m_list;
            size_type x_;
            size_type y_;
            size_type z_;
            size_type m_limit;
            std::atomic_size_t m_current;
            bool m_moved = false;
    };
}

#endif /* GLADOS_BITS_POOL_ALLOCATOR_H_ */
