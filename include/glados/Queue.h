// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_QUEUE_H_
#define GLADOS_QUEUE_H_

#include <condition_variable>
#include <cstddef>
#include <mutex>
#include <queue>
#include <utility>

namespace glados
{

namespace details
{
enum class QueuePolicy
{
	Blocking,
	OverwriteWhenFull,
	DiscardWhenFull
};
}

template <class T>
auto queue_limit(T t) -> std::size_t
{
	return static_cast<std::size_t>(t);
}

template <class Object>
class Queue
{
	public:
	/*
	 * The default constructed Queue has limit 2, hence the member is 2.
	 */
	Queue() : m_limit{10u}, m_count{0u} {}
	explicit Queue(std::size_t limit) : m_limit{limit}, m_count{0u} {}

	/*
	 * Item and Object are of the same type but we need this extra template to make use of the
	 * nice reference collapsing rules
	 */
	template <class Item>
	void push(Item&& item, details::QueuePolicy policy = details::QueuePolicy::Blocking)
	{
		switch(policy)
		{
		case details::QueuePolicy::Blocking:
			return pushBlocking(std::move(item));
		case details::QueuePolicy::DiscardWhenFull:
			return pushDiscard(std::move(item));
		case details::QueuePolicy::OverwriteWhenFull:
			return pushOverwrite(std::move(item));
		}
	}

	template <class Item>
	void pushBlocking(Item&& item)
	{
		auto lock = std::unique_lock<decltype(m_mutex)>{m_mutex};
		if(m_limit != 0u)
		{
			while(m_count >= m_limit)
				m_count_cv.wait(lock);
		}

		m_queue.push(std::forward<Item>(item));

		if(m_limit != 0u)
			++m_count;

		m_item_cv.notify_one();
	}

	template <class Item>
	void pushOverwrite(Item&& item)
	{
		auto lock = std::unique_lock<decltype(m_mutex)>{m_mutex};
		if(m_limit != 0u)
		{
			while(m_count >= m_limit)
			{
				m_queue.pop();
				--m_count;
			}
		}

		m_queue.push(std::forward<Item>(item));

		if(m_limit != 0u)
			++m_count;

		m_item_cv.notify_one();
	}

	template <class Item>
	void pushDiscard(Item&& item)
	{
		auto lock = std::unique_lock<decltype(m_mutex)>{m_mutex};
		if(m_limit != 0u)
		{
			if(m_count >= m_limit)
			{
				return;
			}
		}

		m_queue.push(std::forward<Item>(item));

		if(m_limit != 0u)
			++m_count;

		m_item_cv.notify_one();
	}

	//! take one Object from the Queue.
	//! When synchronous==true (default), this function will block until an item is availible.
	//! When synchronous==false, this function will return asap:
	//!		when the queue is not empty, it will return oldest item
	//!		when the queue is empty, it will return a default constructed item
	Object take(bool synchronous = true)
	{
		auto lock = std::unique_lock<decltype(m_mutex)>{m_mutex};
		if(synchronous)
		{
			/* block until an item is available */
			while(m_queue.empty())
			{
				m_item_cv.wait(lock);
			}
		}
		else
		{
			// if no item is available, return default constructed object
			if(m_queue.empty())
			{
				return Object();
			}
		}
		
		// at this point, an item is guaranteed to be availble
		auto ret = std::move(m_queue.front());
		m_queue.pop();

		if(m_limit != 0u)
		{
			--m_count;
			m_count_cv.notify_one();
		}

		return ret;
	}

	private:
	const std::size_t m_limit;
	std::size_t m_count;
	mutable std::mutex m_mutex;
	std::condition_variable m_item_cv, m_count_cv;
	std::queue<Object> m_queue;
};
}

#endif /* GLADOS_QUEUE_H_ */
