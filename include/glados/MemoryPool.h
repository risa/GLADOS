// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef MEMORYPOOL_H_
#define MEMORYPOOL_H_

#include "Image.h"
#include "Singleton.h"

#include <boost/log/trivial.hpp>

#include <condition_variable>
#include <exception>
#include <memory>
#include <mutex>
#include <vector>

namespace glados
{

template <class MemoryManager>
class Image;

//! This class acts as a Memory pool and initializes memory at program initialization
/**
 *	At program initialization the requesting stage asks for a given number of
 *	elements of a given data type and size. The MemoryPool allocates the memory
 *	and provides during data processing, when a stage asks for it.
 *
 */
template <class MemoryManager>
class MemoryPool
	: public Singleton<MemoryPool<MemoryManager>>
	, MemoryManager
{

	friend class Singleton<MemoryPool<MemoryManager>>;

	public:
	// forward declaration
	using type = glados::Image<MemoryManager>;

	//! Returns memory during data processing to the requesting stage.
	/**
	 * All stages that are registered in MemoryPool can request memory with
	 * this function. If the stage is not registered, an exception will be thrown.
	 * Memory allocation occurs only, if stage did not request enough memory
	 * during registration. In all other cases no allocation, no copy operations
	 * will be performed.
	 *
	 * @param[in] idx stage that requests memory, got an id during registration.
	 *            This id needs to passed to this function.
	 */
	auto requestMemory(size_t idx) -> type
	{
		// std::lock_guard<std::mutex> lock(m_memoryManagerMutex);
		auto lock = std::unique_lock<std::mutex>{m_memoryManagerMutex};
		if(m_memoryPool.size() <= idx)
			throw std::runtime_error("cuda::MemoryPool: Stage needs to be registered first.");
		while(m_memoryPool[idx].empty()) // if all memory for the stage with this idx is given out already
		{
			m_cv.wait(lock); // wait until some becomes availible
		}
		auto ret = std::move(m_memoryPool[idx].back());
		m_memoryPool[idx].pop_back();
		return ret;
	}

	//!	This function reenters the data element in the memory pool.
	/**
	 * This function gets an image, e.g. when image gets out of scope
	 * and stores it in the memory pool vector, where it originally
	 * came from
	 *
	 * @param[in] img Image, that shall be returned into memory pool for reuse
	 *
	 */
	auto returnMemory(type&& img) -> void
	{
		if(m_memoryPool.size() <= img.memoryPoolIndex())
			throw std::runtime_error("cuda::MemoryPool: Stage needs to be registered first.");
		std::lock_guard<std::mutex> lock{m_memoryManagerMutex};
		m_memoryPool[img.memoryPoolIndex()].push_back(std::move(img));
		m_cv.notify_one(); // new memory availible, wake up a starved requestMemory-caller(if any)
	}

	//! This function is called at program initialization, when a stage needs memory during data processing.
	/**
	 * All stages that need memory need to register in MemoryManager.
	 * Stages need to tell, which size of memory they need and how many elements.
	 * The MemoryManager then allocates the memory and manages it.
	 *
	 * @param[in] numberOfElements 	number of elements that shall be allocated by the MemoryManager
	 * @param[in] size				size of memory that needs to be allocated per element
	 *
	 * @return identifier, where
	 *
	 */
	auto registerStage(const int& numberOfElements, const size_t& size) -> size_t
	{
		// lock, to ensure thread safety
		std::lock_guard<std::mutex> lock(m_memoryManagerMutex);
		if(numberOfElements == 0)
		{
			throw std::runtime_error("cuda::MemoryPool: Requested to register empty memory pool.");
		}
		std::vector<type> memory;
		size_t index = m_memoryPool.size(); // get next unused index to ascertain that stage-inexes are given
										   // in ascending order (for size 3, {0,1,2} are taken-> use 3 next)
		for(int i = 0; i < numberOfElements; i++)
		{
			auto ptr = MemoryManager::make_ptr(size);
			auto img = type{size, 0, 0, std::move(ptr)}; // make one of numberOfElements images
			img.setMemPoolIdx(index); // tell the image the index of the stage it belongs to
			memory.push_back(std::move(img));
		}
		m_memoryPool.push_back(
			std::move(memory)); // memory is now a vector of images, of length numberOfElements, enqueue it.
								// This means that the stage with index "index" now has reserved "memory".
		return index;
	}

	//! When the classes are destroyed, this functions frees the allocated memory.
	/**
	 *	@param[in]	idx	idx stage that requests memory, got an id during registration.
	 *            			This id needs to passed to this function.
	 *
	 */
	auto freeMemory(const size_t idx) -> void
	{
		for(auto& ele : m_memoryPool[idx]) // for each img that this stage reserved
		{
			ele.invalid(); // invalidate image
		}
		m_memoryPool[idx].clear(); // clean up Vector<img> , that held the memory for this stage
	}

	private:
	~MemoryPool() = default;

	MemoryPool() = default;

	private:
	std::vector<std::vector<type>>
		m_memoryPool; //!	this vector stores the elements for each stage, read access
					 //! as: m_memoryPool[pipelineStageIdx][imageIDForThatStage]
	mutable std::mutex m_memoryManagerMutex; //! 	the mutex to ensure thread-safety
	std::condition_variable
		m_cv; //!	condition_variable to notify threads that wait for memory when none is availible
};
}

#endif /* MEMORYPOOL_H_ */
