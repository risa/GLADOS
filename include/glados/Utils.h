// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_UTILS_H_
#define GLADOS_UTILS_H_
#pragma once

#include <array>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include <boost/log/trivial.hpp>

#ifdef __linux__
#define FORMAT_PATH(str) \
	std::replace(str.begin(), str.end(), '\\', '/'); \
	if(str.back() != '/') \
	{ \
		str.push_back('/'); \
	}
#elif _WIN32
#define FORMAT_PATH(str) \
	std::replace(str.begin(), str.end(), '/', '\\'); \
	if(str.back() != '\\') \
	{ \
		str.push_back('\\'); \
	}
#endif

namespace glados
{
namespace utils
{

auto setThreadName(const char* threadName) -> void;

class ShellHandle
{
	ShellHandle() = delete;
	
public:
	/* execute shell command and capture output to string */
	static auto exec(const char* cmd) -> std::string;
};

class FileSystemHandle
{
	FileSystemHandle() = delete;

	public:
	static auto createDirectory(std::string path) -> bool;
	static auto createEmptyFile(const std::string& path) -> bool;
	static auto setFilePermissionsWriteAll(const std::string& path) -> bool;
	static auto setFilePermissionsAllAll(const std::string& path) -> bool;
};
}
}

#endif /* GLADOS_UTILS_H_ */
