// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef CUDA_HOSTMEMORYMANAGER_H_
#define CUDA_HOSTMEMORYMANAGER_H_

#include <cstddef>
#include <type_traits>

#include "Memory.h"

namespace glados
{
namespace cuda
{
template <class T, class CopyPolicy = sync_copy_policy>
class HostMemoryManager : public CopyPolicy
{
	public:
	using value_type = T;
	using pointer_type_1D = host_ptr<T, CopyPolicy>;
	using pointer_type_2D = pitched_host_ptr<T, CopyPolicy, std::false_type>;
	using pointer_type_3D = pitched_host_ptr<T, CopyPolicy, std::true_type>;
	using size_type = std::size_t;

	protected:
	inline auto make_ptr(size_type size) -> pointer_type_1D
	{
		return make_host_ptr<value_type, CopyPolicy>(size);
	}

	inline auto make_ptr(size_type width, size_type height) -> pointer_type_2D
	{
		return make_host_ptr<value_type, CopyPolicy>(width, height);
	}

	inline auto make_ptr(size_type width, size_type height, size_type depth) -> pointer_type_3D
	{
		return make_host_ptr<value_type, CopyPolicy>(width, height, depth);
	}

	template <typename Source>
	inline auto copy(pointer_type_1D& dest, Source& src, size_type size) -> void
	{
		CopyPolicy::copy(dest, src, size);
	}

	template <typename Source>
	inline auto copy(pointer_type_2D& dest, Source& src, size_type width, size_type height) -> void
	{
		CopyPolicy::copy(dest, src, width, height);
	}

	template <typename Source>
	inline auto copy(pointer_type_3D& dest, Source& src, size_type width, size_type height, size_type depth)
		-> void
	{
		CopyPolicy::copy(dest, src, width, height, depth);
	}

	~HostMemoryManager() = default;
};
}
}

#endif /* CUDA_HOSTMEMORYMANAGER_H_ */
