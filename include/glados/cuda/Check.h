// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef CUDA_CHECK_H_
#define CUDA_CHECK_H_

#include <stdexcept>

#ifndef __CUDACC__
#include <cuda_runtime.h>
#endif

#include <cufft.h>
#include <cusparse.h>
#ifdef NVJPEG_AVAILABLE
#include <nvjpeg.h>
#endif

#define CHECK(x) glados::cuda::check(x, __FILE__, __LINE__)
#define CHECK_CUFFT(x) glados::cuda::checkCufft(x, __FILE__, __LINE__)
#define CHECK_CUSPARSE(x) glados::cuda::checkCusparse(x, __FILE__, __LINE__)
#define CHECK_NVJPEG(x) glados::cuda::checkNvjpeg(x, __FILE__, __LINE__)

namespace glados
{
namespace cuda
{
namespace detail
{
inline auto checkCudaError(cudaError_t err, const char* file, int line) -> void
{
	if(err != cudaSuccess)
	{
		std::string errStr = "CUDA call failed at " + std::string(file) + ":" + std::to_string(line) + ": "
			+ std::string(cudaGetErrorString(err));
		BOOST_LOG_TRIVIAL(fatal) << errStr;
		throw std::runtime_error{errStr};
	}
}

inline auto getCufftErrorString(cufftResult result) -> std::string
{
	switch(result)
	{
	case CUFFT_SUCCESS:
		return "The cuFFT operation was successful";
	case CUFFT_INVALID_PLAN:
		return "cuFFT was passed an invalid plan handle";
	case CUFFT_ALLOC_FAILED:
		return "cuFFT failed to allocate GPU or CPU memory";
	case CUFFT_INVALID_TYPE:
		return "Invalid type";
	case CUFFT_INVALID_VALUE:
		return "Invalid pointer or parameter";
	case CUFFT_INTERNAL_ERROR:
		return "Driver or internal cuFFT library error";
	case CUFFT_EXEC_FAILED:
		return "Failed to execute an FFT on the GPU";
	case CUFFT_SETUP_FAILED:
		return "The cuFFT library failed to initialize";
	case CUFFT_INVALID_SIZE:
		return "User specified an invalid transform size";
	case CUFFT_UNALIGNED_DATA:
		return "Unaligned data";
	case CUFFT_INCOMPLETE_PARAMETER_LIST:
		return "Missing parameters in call";
	case CUFFT_INVALID_DEVICE:
		return "Execution of plan was on different GPU than plan creation";
	case CUFFT_PARSE_ERROR:
		return "Internal plan database error";
	case CUFFT_NO_WORKSPACE:
		return "No workspace has been provided prior to plan execution";
	case CUFFT_NOT_IMPLEMENTED:
		return "This feature was not implemented for your cuFFT version";
	case CUFFT_LICENSE_ERROR:
		return "NVIDIA license required. The file was either not found, is out of data, or otherwise invalid";
	default:
		return "Unknown error";
	}
}

inline auto checkCufftError(cufftResult result, const char* file, int line) -> void
{
	if(result != CUFFT_SUCCESS)
	{
		throw std::runtime_error{"cuFFT call failed at " + std::string(file) + ":" + std::to_string(line)
			+ ": " + getCufftErrorString(result)};
	}
}

inline auto getCusparseErrorString(cusparseStatus_t stat) -> std::string
{
	switch(stat)
	{
	case CUSPARSE_STATUS_SUCCESS:
		return "The operation completed successfully.";
	case CUSPARSE_STATUS_NOT_INITIALIZED:
		return "The cuSPARSE library was not initialized.";
	case CUSPARSE_STATUS_ALLOC_FAILED:
		return "Resource allocation failed inside the cuSPARSE library.";
	case CUSPARSE_STATUS_INVALID_VALUE:
		return "An unsupported value or parameter was passed to the function.";
	case CUSPARSE_STATUS_ARCH_MISMATCH:
		return "The function requires a feature absent from the device architecture.";
	case CUSPARSE_STATUS_MAPPING_ERROR:
		return "An access to GPU memory space failed.";
	case CUSPARSE_STATUS_EXECUTION_FAILED:
		return "The GPU program failed to execute.";
	case CUSPARSE_STATUS_INTERNAL_ERROR:
		return "An internal cuSPARSE operation failed.";
	case CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED:
		return "The matrix type is not supported by this function.";
	case CUSPARSE_STATUS_ZERO_PIVOT:
		return "An entry of the matrix is either structural zero or numerical zero (singular block)";
	default:
		return "Unknown error";
	}
}

inline auto checkCusparseError(cusparseStatus_t stat, const char* file, int line) -> void
{
	if(stat != CUSPARSE_STATUS_SUCCESS)
	{
		throw std::runtime_error{"cuSPARSE call failed at " + std::string{file} + ":" + std::to_string(line)
			+ ": " + getCusparseErrorString(stat)};
	}
}
#ifdef NVJPEG_AVAILABLE
inline auto getNvjpegErrorString(nvjpegStatus_t stat) -> std::string
{
	switch(stat)
	{
	case NVJPEG_STATUS_SUCCESS:
		return "The operation completed successfully.";
	case NVJPEG_STATUS_NOT_INITIALIZED:
		return "The library handle was not initialized. A call to nvjpegCreate() is required to initialize "
			   "the handle.";
	case NVJPEG_STATUS_INVALID_PARAMETER:
		return "Wrong parameter was passed. For example, a null pointer as input data, or an image index not "
			   "in the allowed range.";
	case NVJPEG_STATUS_BAD_JPEG:
		return "Cannot parse the JPEG stream. Check that the encoded JPEG stream and its size parameters are "
			   "correct.";
	case NVJPEG_STATUS_JPEG_NOT_SUPPORTED:
		return "Attempting to decode a JPEG stream that is not supported by the nvJPEG library.";
	case NVJPEG_STATUS_ALLOCATOR_FAILURE:
		return "The user-provided allocator functions, for either memory allocation or for releasing the "
			   "memory, returned a non-zero code.";
	case NVJPEG_STATUS_EXECUTION_FAILED:
		return "Error during the execution of the device tasks.";
	case NVJPEG_STATUS_ARCH_MISMATCH:
		return "The device capabilities are not enough for the set of input parameters provided.";
	case NVJPEG_STATUS_INTERNAL_ERROR:
		return "Error during the execution of the device tasks.";
	case NVJPEG_STATUS_IMPLEMENTATION_NOT_SUPPORTED:
		return "Not supported.";
	// case NVJPEG_STATUS_INCOMPLETE_BITSTREAM:
	//	 return "Bitstream input data incomplete.";
	default:
		return "Unknown error";
	}
}

inline auto checkNvjpegError(nvjpegStatus_t stat, const char* file, int line) -> void
{
	if(stat != NVJPEG_STATUS_SUCCESS)
	{
		throw std::runtime_error{"nvJPEG call failed at " + std::string{file} + ":" + std::to_string(line)
			+ ": " + getNvjpegErrorString(stat)};
	}
}
#endif
} // namespace detail


inline auto check(cudaError_t err, const char* file, int line) -> void
{
	detail::checkCudaError(err, file, line);
}

inline auto checkCufft(cufftResult res, const char* file, int line) -> void
{
	detail::checkCufftError(res, file, line);
}

inline auto checkCusparse(cusparseStatus_t stat, const char* file, int line) -> void
{
	detail::checkCusparseError(stat, file, line);
}

#ifdef NVJPEG_AVAILABLE
inline auto checkNvjpeg(nvjpegStatus_t stat, const char* file, int line) -> void
{
	detail::checkNvjpegError(stat, file, line);
}
#endif
}
}

#endif /* CUDA_CHECK_H_ */
