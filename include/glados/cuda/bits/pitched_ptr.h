// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUDA_BITS_PITCHED_PTR_H_
#define GLADOS_CUDA_BITS_PITCHED_PTR_H_

#include <cstddef>

namespace glados
{
    namespace cuda
    {
        template <typename T>
        class pitched_ptr
        {
            public:
                explicit pitched_ptr(T* p, std::size_t ptr_pitch) noexcept : m_ptr{p}, m_pitch{ptr_pitch} {}
                explicit pitched_ptr(std::nullptr_t) noexcept : m_ptr{nullptr}, m_pitch{0} {}

                auto ptr() const noexcept -> T* { return m_ptr; }
                auto pitch() const noexcept -> std::size_t { return m_pitch; }

            private:
                T* m_ptr;
                std::size_t m_pitch;
        };

        template <class T>
        auto operator==(const pitched_ptr<T>& x, std::nullptr_t) -> bool
        {
            return x.ptr() == nullptr;
        }

        template <class T>
        auto operator==(std::nullptr_t, const pitched_ptr<T>& y) -> bool
        {
            return nullptr == y.ptr();
        }

        template <class T>
        auto operator!=(const pitched_ptr<T>&x, std::nullptr_t) -> bool
        {
            return !(x == nullptr);
        }

        template <class T>
        auto operator!=(std::nullptr_t, const pitched_ptr<T>& y) -> bool
        {
            return !(nullptr == y);
        }
    }
}

#endif /* GLADOS_CUDA_BITS_PITCHED_PTR_H_ */
