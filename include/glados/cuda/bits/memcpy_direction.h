// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUDA_BITS_MEMCPY_DIRECTION_H_
#define GLADOS_CUDA_BITS_MEMCPY_DIRECTION_H_

#ifndef __CUDACC__
#include <cuda_runtime.h>
#endif

#include <glados/bits/memory_location.h>

namespace glados
{
    namespace cuda
    {
        namespace detail
        {
            template <memory_location d, memory_location s>
            struct memcpy_direction {};

            template <>
            struct memcpy_direction<memory_location::device, memory_location::host>
            {
                static constexpr auto value = cudaMemcpyHostToDevice;
            };

            template <>
            struct memcpy_direction<memory_location::host, memory_location::device>
            {
                static constexpr auto value = cudaMemcpyDeviceToHost;
            };

            template <>
            struct memcpy_direction<memory_location::host, memory_location::host>
            {
                static constexpr auto value = cudaMemcpyHostToHost;
            };

            template <>
            struct memcpy_direction<memory_location::device, memory_location::device>
            {
                static constexpr auto value = cudaMemcpyDeviceToDevice;
            };
        }
    }
}

#endif /* GLADOS_CUDA_BITS_MEMCPY_DIRECTION_H_ */
