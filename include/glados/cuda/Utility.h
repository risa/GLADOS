// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUDA_UTILITY_H_
#define GLADOS_CUDA_UTILITY_H_

#include <cstddef>
#include <vector>

#ifndef __CUDACC__
#include <cuda_runtime.h>
#endif

#include <glados/cuda/bits/throw_error.h>

namespace glados
{
    namespace cuda
    {
        inline auto set_device(int d) -> void
        {
            auto err = cudaSetDevice(d);
            if(err != cudaSuccess)
                detail::throw_error(err);
        }

        inline auto get_device() -> int
        {
            auto d = int{};
            auto err = cudaGetDevice(&d);
            if(err != cudaSuccess)
                detail::throw_error(err);

            return d;
        }

        inline auto get_device_count() -> int
        {
            auto d = int{};
            auto err = cudaGetDeviceCount(&d);
            if(err != cudaSuccess)
                detail::throw_error(err);

            return d;
        }

        inline auto get_device_properties(int device) -> cudaDeviceProp
        {
            auto prop = cudaDeviceProp{};
            auto err = cudaGetDeviceProperties(&prop, device);
            if(err != cudaSuccess)
                detail::throw_error(err);

            return prop;
        }

        inline auto set_valid_devices(int* devices, std::size_t len) -> void
        {
            auto len_i = static_cast<int>(len);
            auto err = cudaSetValidDevices(devices, len_i);
            if(err != cudaSuccess)
                detail::throw_error(err);
        }

        inline auto set_valid_devices(std::vector<int>& devices) -> void
        {
            set_valid_devices(devices.data(), devices.size());
        }

        inline auto create_stream() -> cudaStream_t
        {
            auto s = cudaStream_t{};
            auto err = cudaStreamCreate(&s);
            if(err != cudaSuccess)
                detail::throw_error(err);

            return s;
        }

        inline auto create_concurrent_stream() -> cudaStream_t
        {
            auto s = cudaStream_t{};
            auto err = cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
            if(err != cudaSuccess)
                detail::throw_error(err);

            return s;
        }

        inline auto synchronize_stream(cudaStream_t stream = 0) -> void
        {
            auto err = cudaStreamSynchronize(stream);
            if(err != cudaSuccess)
                detail::throw_error(err);
        }
    }
}

#endif /* GLADOS_CUDA_UTILITY_H_ */
