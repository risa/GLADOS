// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUDA_COORDINATES_H_
#define GLADOS_CUDA_COORDINATES_H_

namespace glados
{
namespace cuda
{
inline __device__ auto getX() -> unsigned int { return blockIdx.x * blockDim.x + threadIdx.x; }

inline __device__ auto getY() -> unsigned int { return blockIdx.y * blockDim.y + threadIdx.y; }

inline __device__ auto getZ() -> unsigned int { return blockIdx.z * blockDim.z + threadIdx.z; }
}
}

#endif /* GLADOS_CUDA_COORDINATES_H_ */
