// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUDA_ALGORITHM_H_
#define GLADOS_CUDA_ALGORITHM_H_

#include <cstddef>
#include <utility>
#include <type_traits>

namespace glados
{
    namespace cuda
    {
        template <class SyncPolicy, class D, class S, class... Args>
        auto copy(SyncPolicy&& policy, D& dst, const S& src, Args&&... args) -> void
        {
            policy.copy(dst, src, std::forward<Args>(args)...);
        }

        /**
         * Note that fill() will apply value to the individual bytes of the data, not the elements
         */
        template <class SyncPolicy, class P, class... Args>
        auto fill(SyncPolicy&& policy, P& p, int value, Args&&... args) -> void
        {
            policy.fill(p, value, std::forward<Args>(args)...);
        }
    }
}

#endif /* GLADOS_CUDA_ALGORITHM_H_ */
