// SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de)

#ifndef GLADOS_IMAGE_H_
#define GLADOS_IMAGE_H_

#include "MemoryPool.h"
#include "Properties.h"

#include <boost/log/trivial.hpp>
#include <boost/stacktrace.hpp>

#include <algorithm>
#include <any>
#include <cstddef>
#include <cstdint>
#include <map>
#include <memory>
#include <time.h>
#include <tuple>
#include <type_traits>
#include <utility>

namespace glados
{
template <class MemoryManager>
class Image : public MemoryManager
{
	public:
	using value_type = typename MemoryManager::value_type;
	using pointer_type = typename MemoryManager::pointer_type_1D;
	using size_type = typename MemoryManager::size_type;

	private:
#ifdef __linux__
	using m_clock = std::chrono::high_resolution_clock;
#elif _WIN32
	using m_clock = std::chrono::system_clock;
#endif

	public:
	Image() noexcept : m_data{nullptr}, m_memoryPoolIndex{0}, m_valid{false}, m_properties{} {}

	~Image()
	{
		if(m_valid)
		{
			// if valid image goes out of scope, return it to MemoryPool -> automatic reuse
			removeProperties();
			MemoryPool<MemoryManager>::getInstance().returnMemory(std::move(*this));
		}
	}

	Image(size_type size, size_type idx = 0, size_type planeID = 0, pointer_type img_data = nullptr)
		: MemoryManager()
		, m_data{std::move(img_data)}
		, m_memoryPoolIndex{0}
		, m_valid{true}
		, m_size{size}
		, m_properties{}
	{
		setIdx(idx);
		setPlane(planeID);

		if(m_data == nullptr)
			m_data = MemoryManager::make_ptr(size);
	}

	Image(const Image& other) : MemoryManager(other), m_valid{other.m_valid}, m_size{other.m_size}, m_properties(other.m_properties)
	{
		if(other.m_data == nullptr)
			m_data = nullptr;
		else
		{
			m_data = MemoryManager::make_ptr(size());
			MemoryManager::copy(m_data, other.m_data, size());
		}
	}

	auto setIdx(size_type idx) -> void
	{
		std::ignore = setProperty("index", idx, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
	}

	auto setPlane(size_type plane) -> void
	{
		std::ignore = setProperty(
			"plane", plane, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
	}

	auto setMemPoolIdx(size_type idx) -> void { m_memoryPoolIndex = idx; }

	auto setStart(std::chrono::time_point<m_clock> start) -> void
	{
		std::ignore =
			setProperty("start", start, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
	}

	auto duration(std::string from = "start") -> double
	{
		std::chrono::time_point<m_clock> start;
		if (getProperty(from, start) == glados::details::PropertyReturnCode::Success)
			return std::chrono::duration<double, std::milli>(m_clock::now() - start).count();

		throw std::runtime_error("glados::Image: '" + from + "' not set before calling 'duration'.");
	}

	auto invalid() -> void { m_valid = false; }

	template <typename U>
	auto operator=(const Image<U>& rhs) -> Image&
	{
		m_valid = rhs.valid();
		m_properties = rhs.properties();
		m_size = rhs.size();

		if(rhs.container() == nullptr)
			m_data = nullptr;
		else
		{
			m_data.reset(); // delete old content if any
			m_data = MemoryManager::make_ptr(m_size);
			MemoryManager::copy(m_data, rhs.container(), m_size);
		}

		return *this;
	}

	Image(Image&& other) noexcept
		: MemoryManager(std::move(other))
		, m_data{std::move(other.m_data)}
		, m_memoryPoolIndex{other.m_memoryPoolIndex}
		, m_valid{other.m_valid}
		, m_size{other.m_size}
	{
		m_properties.clear();
		m_properties.merge(other.m_properties); // this will move all properties from other.m_properties, essentially clearing it
		other.m_valid = false; // invalid after we moved its data
	}

	auto operator=(Image&& rhs) noexcept -> Image&
	{
		m_data = std::move(rhs.m_data);
		m_valid = rhs.m_valid;
		m_size = rhs.m_size;
		m_memoryPoolIndex = rhs.m_memoryPoolIndex;
		
		m_properties.clear();
		m_properties.merge(rhs.m_properties);

		MemoryManager::operator=(std::move(rhs));

		rhs.m_valid = false;

		return *this;
	}

	auto size() const -> size_type { return m_size; }

	/*
	 * returns a non-owning pointer to the data. Do not delete this pointer as the Image object will take
	 * care of the memory.
	 */
	auto data() const noexcept -> value_type* { return m_data.get(); }

	auto index() const -> size_type
	{
		if (!m_valid) {
			handlePropertyAccessOnInvalidImage("index");
		}
		size_type index;
		if(getProperty("index", index) == glados::details::PropertyReturnCode::Success)
			return index;

		BOOST_LOG_TRIVIAL(fatal) << "Required property not found. Here is the backtrace: \n"
							<< boost::stacktrace::stacktrace();

		throw std::runtime_error("glados::Image: No valid property 'index' on image.");
	}

	auto memoryPoolIndex() const noexcept -> size_type { return m_memoryPoolIndex; }

	auto pitch() const noexcept -> size_type { return m_data.pitch(); }

	auto plane() const -> size_type
	{
		if(!m_valid)
		{
			handlePropertyAccessOnInvalidImage("plane");
		}
		size_type plane;
		if(getProperty("plane", plane) == glados::details::PropertyReturnCode::Success)
			return plane;
		BOOST_LOG_TRIVIAL(fatal) << "Required property not found. Here is the backtrace: \n"
								 << boost::stacktrace::stacktrace();
		throw std::runtime_error("glados::Image: No valid property 'plane' on image.");
	}

	auto start() -> std::chrono::time_point<m_clock>
	{
		if(!m_valid)
		{
			handlePropertyAccessOnInvalidImage("start");
		}
		std::chrono::time_point<m_clock> start;
		if(getProperty("start", start) == glados::details::PropertyReturnCode::Success)
			return start;
		BOOST_LOG_TRIVIAL(fatal) << "Required property not found. Here is the backtrace: \n"
								 << boost::stacktrace::stacktrace();
		throw std::runtime_error("glados::Image: No valid property 'start' on image.");
	}

	auto valid() const noexcept -> bool { return m_valid; }

	auto properties() const noexcept -> std::map<std::string, std::any> { return m_properties; }

	/*
	 * return the underlying pointer
	 */
	auto container() const noexcept -> const pointer_type& { return m_data; }

	/*
	 * custom property handling
	 */
	template <typename T>
	[[nodiscard]] auto setProperty(std::string key, T value, glados::details::PropertyOverwritePolicy overwritePolicy = glados::details::PropertyOverwritePolicy::DoNotOverwrite) noexcept
		-> glados::details::PropertyReturnCode
	{
		static_assert(std::is_convertible<T, std::any>::value,
			"glados::Image: Property of given type cannot be converted into std::any.");

		if(m_properties.count(key))
		{
			if(overwritePolicy == glados::details::PropertyOverwritePolicy::DoNotOverwrite)
			{
				return glados::details::PropertyReturnCode::NotOverwritten;
			}
			if (m_properties.at(key).type() != typeid(T) && overwritePolicy != glados::details::PropertyOverwritePolicy::OverwriteValueAndType)
			{
				return glados::details::PropertyReturnCode::TypeMismatch;
			}
			m_properties[key] = value;
			return glados::details::PropertyReturnCode::Success;
		}
		else
		{
			m_properties.emplace(key, value);
			return glados::details::PropertyReturnCode::Success;
		}
	}

	template <typename T>
	[[nodiscard]] auto getProperty(std::string key, T& value) const noexcept -> glados::details::PropertyReturnCode
	{
		if(!m_valid)
			handlePropertyAccessOnInvalidImage(key);

		if(!m_properties.count(key))
			return glados::details::PropertyReturnCode::NotFound;

		if(m_properties.at(key).type() != typeid(T))
			return glados::details::PropertyReturnCode::TypeMismatch;

		try
		{
			value = std::any_cast<T>(m_properties.at(key));
		}
		catch(const std::bad_any_cast&)
		{
			return glados::details::PropertyReturnCode::TypeMismatch;
		}

		return glados::details::PropertyReturnCode::Success;
	}

	auto removeProperty(std::string key) -> void { m_properties.erase(key); }

	auto removeProperties() -> void { m_properties.clear(); }

	auto hasProperty(std::string key) const noexcept -> bool { return (bool)m_properties.count(key); };

	auto setProperties(std::map<std::string, std::any> properties) { m_properties = properties; };

	auto getPropertyKeys() const -> const std::vector<std::string> { 
		std::vector<std::string> keys;
		for(const auto& [k, v] : m_properties)
			keys.push_back(k);

		return keys;
	}

	private:
	pointer_type m_data;
	size_type m_memoryPoolIndex;
	bool m_valid;
	size_type m_size;
	std::map<std::string, std::any> m_properties;

	[[noreturn]] void handlePropertyAccessOnInvalidImage(std::string key) const {
		BOOST_LOG_TRIVIAL(fatal)
			<< "glados::Image: Cannot access property '" << key
			<< "' on invalid Image. Note: Images are invalidated after moving their contents.";
		throw std::runtime_error("glados::Image: Cannot access property of invalid Image.");
	}
};
}

#endif /* GLADOS_IMAGE_H_ */
