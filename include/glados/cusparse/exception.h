// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_CUSPARSE_EXCEPTION_H_
#define GLADOS_CUSPARSE_EXCEPTION_H_

#include <exception>
#include <stdexcept>

namespace glados
{
    namespace cusparse
    {
        /*
         * CUSPARSE_STATUS_ALLOC_FAILED
         */
        class bad_alloc : public std::exception
        {
            bad_alloc() noexcept = default;
            virtual ~bad_alloc() = default;

            auto operator=(const bad_alloc&) noexcept -> bad_alloc& { return *this; }
            auto what() const noexcept -> const char*
            {
                return "Resource allocation failed inside the cuSPARSE library";
            }
        }

        /*
         * CUSPARSE_STATUS_INVALID_VALUE
         * CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED
         * CUSPARSE_STATUS_ZERO_PIVOT
         */
        class invalid_argument : public std::invalid_argument
        {
            public:
                using std::invalid_argument::invalid_argument;
                virtual ~invalid_argument() = default;
        };

        /*
         * CUSPARSE_STATUS_NOT_INITIALIZED
         * CUSPARSE_STATUS_ARCH_MISMATCH
         * CUSPARSE_STATUS_MAPPING_ERROR
         * CUSPARSE_STATUS_EXECUTION_FAILED
         * CUSPARSE_STATUS_INTERNAL_ERROR
         */
        class runtime_error : public std::runtime_error
        {
            public:
                using std::runtime_error::runtime_error;
                virtual ~runtime_error() = default;
        };
    }
}

#endif /* GLADOS_CUSPARSE_EXCEPTION_H_ */
