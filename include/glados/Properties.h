// SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author:
// Dominic Windisch (d.windisch@hzdr.de)

#ifndef GLADOS_PROPERTIES_H_
#define GLADOS_PROPERTIES_H_
#pragma once

namespace glados {
namespace details {
enum class PropertyReturnCode
{
	NotOverwritten,
	NotFound,
	Success,
	TypeMismatch
};

enum class PropertyOverwritePolicy 
{
	DoNotOverwrite,
	OverwriteValue,
	OverwriteValueAndType
};
}
}



#endif /* GLADOS_PROPERTIES_H_ */
