// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_PIPELINE_PIPELINE_H_
#define GLADOS_PIPELINE_PIPELINE_H_

#include <memory>
#include <thread>
#include <utility>
#include <vector>

#include "GenericStage.h"
#include "Port.h"

namespace glados
{
namespace pipeline
{
class Pipeline
{
	public:
	//! Connects the two given pipeline stages
	/**
	 * @param[in]  first     the pipeline stage which shall output data to `second`
	 * @param[in] second    the pipeline stage which takes in data from `first`
	 */
	template <class First, typename Arg>
	auto connect(First first, Arg&& second) -> void
	{
		first.get()->connectTo(second, 0);
	}

	//! Connects the output streams of the first pipeline with one input stream from each following pipeline
	//! stage
	/**
	 * @param[in]  first      the pipeline stage which shall output data to all further stages
	 * @param[in]  second     the pipeline stage which takes in data from the next unused output stream of
	 * `first`
	 * @param[in]  further    the pipeline stage(s) which takes in data from `first`
	 */
	template <class First, typename Arg, typename... Args>
	auto connect(First first, Arg&& second, Args&&... further) -> void
	{
		using port_type = typename First::element_type::output_type;
		auto port = std::unique_ptr<Port<port_type>>(new Port<port_type>);
		port->attach(second, 0);
		first->attach(std::move(port));
		return connect(first, std::forward<Args>(further)...);
	}

	//! Connects the output streams of the first pipeline with the specific input stream of the following
	//! pipeline stage
	/**
	 * @param[in]  src     the pipeline stage which shall output data to all further stages
	 * @param[in]  dest    the pipeline stage(s) which takes in data from `first`
	 * @param[in] inputIdx     index of the `dest` input stream
	 */
	template <class Dest, typename Src>
	auto connectSpecificInput(Src src, Dest&& dest, int inputIdx) -> void
	{
		try
		{
			src.get()->connectTo(dest, inputIdx);
		}
		catch(...)
		{
			throw;
		}
	}

	template <class PipelineStage, typename... Args>
	auto create(Args&&... args) -> std::shared_ptr<PipelineStage>
	{
		auto stage = std::make_shared<PipelineStage>(std::forward<Args>(args)...);
		m_stages.emplace_back(stage);
		return stage; // remove std::move(stage) to comply with NRVO. The compiler will automatically move the
					  // locally created variable stage.
	}

	auto runAll() -> void
	{
		for(auto st : m_stages)
		{
			m_stage_threads.emplace_back(&GenericStage::run, st);
		}
	}

	template <class Stage>
	auto run(Stage stage) -> void
	{
		m_stage_threads.emplace_back(&Stage::element_type::run, stage);
	}

	template <class Stage, class... Stages>
	auto run(Stage stage, Stages... stages) -> void
	{
		run(stage);
		run(stages...);
	}

	auto wait() -> void
	{
		for(auto&& t : m_stage_threads)
			t.join();
	}

	private:
	std::vector<std::thread> m_stage_threads;
	std::vector<std::shared_ptr<glados::pipeline::GenericStage>> m_stages;
};

}
}

#endif /* GLADOS_PIPELINE_PIPELINE_H_ */
