// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_GENERIC_STAGE_H
#define PIPELINE_GENERIC_STAGE_H

#include "../observer/Subject.h"
#include "../Utils.h" // utils::setThreadName

namespace glados
{
namespace pipeline
{

class GenericStage : public Observer
{
	public:
	virtual ~GenericStage() {}
	virtual void run() = 0;
	virtual void connectTo(std::shared_ptr<GenericStage> other, int portIdx) = 0;
};

}
}

#endif /*PIPELINE_GENERIC_STAGE_H*/
