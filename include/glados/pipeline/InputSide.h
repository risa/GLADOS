// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_INPUTSIDE_H_
#define PIPELINE_INPUTSIDE_H_

#include <utility>

#include "../Queue.h"

namespace glados
{
namespace pipeline
{
template <class InputType>
class InputSide
{
	public:

	//! registers one new input item, in the Queue identified by inputIdx
	auto input(InputType&& in, int inputIdx) -> void
	{
		m_input_queues[inputIdx].push(std::forward<InputType&&>(in));
	}

	protected:
	std::vector<Queue<InputType>> m_input_queues; //! input Queues, one glados::pipeline::Port exists for each Queue
};
}
}

#endif /* PIPELINE_INPUTSIDE_H_ */
