// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_PIPELINE_TASK_QUEUE_H_
#define GLADOS_PIPELINE_TASK_QUEUE_H_

#include <mutex>
#include <queue>
#include <utility>

namespace glados
{
    namespace pipeline
    {
        template <class TaskT>
        class task_queue
        {
            public:
                task_queue(const std::queue<TaskT>& queue)
                : m_queue{queue}
                {}

                auto push(TaskT&& t) -> void
                {
                    auto&& lock = std::lock_guard<std::mutex>{m_mutex};
                    m_queue.push(std::move(t));
                }

                auto pop() -> TaskT
                {
                    auto&& lock = std::lock_guard<std::mutex>{m_mutex};

                    auto ret = std::move(m_queue.front());
                    m_queue.pop();

                    return ret;
                }

                auto empty() const -> bool
                {
                    return m_queue.empty();
                }

            private:
                std::queue<TaskT> m_queue;
                std::mutex m_mutex;
        };
    }
}

#endif /* GLADOS_PIPELINE_TASK_QUEUE_H_ */
