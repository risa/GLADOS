// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_PIPELINE_STAGE_H_
#define GLADOS_PIPELINE_STAGE_H_

#include <thread>
#include <utility>

#include "../Image.h"

#include "../observer/Subject.h"
#include "GenericStage.h"
#include "InputSide.h"
#include "OutputSide.h"

namespace glados
{
namespace pipeline
{
template <class Implementation>
class Stage
	: public InputSide<typename Implementation::input_type> // use input_type from Implementation
	, public OutputSide<typename Implementation::output_type> // use output_type from Implementation
	, public Implementation
	, public GenericStage
{
	public:
	using input_type = typename Implementation::input_type;
	using output_type = typename Implementation::output_type;

	public:
	template <typename... Args>
	Stage(Args&&... args)
		: InputSide<input_type>()
		, OutputSide<output_type>()
		, Implementation(std::forward<Args>(args)..., 1, 1)// if no numberOfInputs and numberOfOutputs is provided, default to 1 for both
	{
		m_numberOfOutputs = 1;
		m_numberOfInputs = 1;
		this->m_input_queues = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
	}

	template <typename... Args>
	Stage(int numberOfInputs, int numberOfOutputs, Args&&... args)
		: InputSide<input_type>()
		, OutputSide<output_type>()
		, Implementation(std::forward<Args>(args)..., numberOfOutputs, numberOfInputs)
	{
		m_numberOfInputs = numberOfInputs;
		m_numberOfOutputs = numberOfOutputs;
		this->m_input_queues = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
	}

	auto run() -> void
	{
		glados::utils::setThreadName("processStage");
		std::vector<std::thread> push_threads;
		std::vector<std::thread> take_threads;
		for(int i = 0; i < m_numberOfInputs; i++)// for each input
		{
			// create one thread that takes items from input queue i and applies Implementation::process on the item
			push_threads.push_back(std::thread{&Stage::push, this, i});
		}
		for(int i = 0; i < m_numberOfOutputs; i++)// for each output
		{
			// create one thread that waits for Implementation::process to complete items and then forwards them to the ports connected at the output of this stage. This in turn will enqueue the item in the InputQueue of the Stages that are connected to the Ports receiving End.
			// In other words, the Flow is as follows:
			// This Stage -> Port of the next stage -> InputQueue of next Stage
			// since there can be multiple outputs, i is used to distinguish them (we launch one thread per output in a loop)
			take_threads.push_back(std::thread{&Stage::take, this, i});
		}

		//at this point, all in/outputs have a corresponding thread that is running and will continue to work until an invalid item arrives.

		for(int i = 0; i < m_numberOfInputs; i++)//wait for all previously launched push threads to finish
		{
			push_threads[i].join();
		};
		for(int i = 0; i < m_numberOfOutputs; i++)//wait for all previously launched take threads to finish
		{
			take_threads[i].join();
		};
	}

	//! takes items from one Input queue of this stage and processes them until an invalid item (the poisonous pill) arrives.
	//! which InputQueue is used is indicated by inputIdx.
	auto push(int inputIdx = 0) -> void
	{
		glados::utils::setThreadName("push");
		while(true)
		{
			auto img = this->m_input_queues[inputIdx].take(); //get item from the inputQueue No. inputIdx of this Stage. This will block if no item is immediately availible.
			if(img.valid())
				Implementation::process(std::move(img), inputIdx);// process the item
			else
			{
				Implementation::process(std::move(img), inputIdx);// forward poisonous pill to kill later stages
				break;// received poisonous pill, time to die
			}
		}
	}

	//! waits for Implementation::process to finish an item and forwards it to the ports that receive from this Stage.
	auto take(int outputIdx = 0) -> void
	{
		glados::utils::setThreadName("take");
		while(true)
		{
			auto result = Implementation::wait(outputIdx);// wait for Implementation::process to finish one item. This will block if no item is immediately availible.
			if(result.valid())
			{
				this->output(std::move(result), outputIdx);// forward it to the connected Port with id outputIdx
			}
			else
			{
				this->output(std::move(result), outputIdx);// forward poisonous pill to kill later stages
				break;// received poisonous pill, time to die
			}
		}
	}

	auto update(Subject* s) -> void { Implementation::update(s); }

	//! connects this Stage to its Successor, m_other.
	//! inputIdx decides to which InputQueue of m_other the output of this stage will be forwarded.
	void connectTo(std::shared_ptr<GenericStage> m_other, int inputIdx) override
	{
		using port_type = output_type;
		auto port = std::unique_ptr<Port<port_type>>(new Port<port_type>);
		auto other = std::dynamic_pointer_cast<InputSide<output_type>>(m_other); //get the input part of the "m_other"-Stage, save it as "other"
		if(other == nullptr)
		{
			throw std::runtime_error("input/output type mismatch.");
		}

		port->attach(other, inputIdx); //tell the port to forward the data it receives to InputQueue No. "inputIdx" of the stage "other".
		this->attach(std::move(port)); //tell this stage to output its data to the port (effectively forwarding it to "other" aswell)
	}

	private:
	int m_numberOfInputs; //! How many Inputs does this Stage have (for each Input there is one Input Queue)
	int m_numberOfOutputs; //! How many Outputs are there (for each Output, we know a Port [if this condition is violated, OutputSide::output() will throw])
};
}
}

#endif /* GLADOS_PIPELINE_STAGE_H_ */
