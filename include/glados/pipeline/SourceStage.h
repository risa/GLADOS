// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_SOURCESTAGE_H_
#define PIPELINE_SOURCESTAGE_H_

#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/log/trivial.hpp>

#include "../Image.h"
#include "../observer/Subject.h"
#include "GenericStage.h"

#include "OutputSide.h"

namespace glados
{
namespace pipeline
{
template <class ImageLoader>
class SourceStage
	: public ImageLoader
	, public OutputSide<typename ImageLoader::output_type>
	, public GenericStage
{
	public:
	using output_type = typename ImageLoader::output_type;

	public:
	template <typename... Args>
	SourceStage(const std::string& configPath, const std::string& parameterSet, Args&&... args)
		: ImageLoader(std::forward<Args>(args)..., configPath, parameterSet)
		, OutputSide<output_type>()
		, m_done{false}
		, m_num{0u}
		, m_path{configPath}
		, m_parameterSet{parameterSet}
	{
	}

	auto run() -> void
	{
		glados::utils::setThreadName("sourceStage");
		while(true)
		{
			auto img = ImageLoader::loadImage();
			if(!img.valid())
			{
				// all images loaded, send poisonous pill
				this->output(output_type());
				m_done = true;
				break;
			}
			this->output(std::move(img));
			++m_num;
		}
	}

	auto update(Subject* s) -> void { ImageLoader::update(s); }

	void connectTo(std::shared_ptr<GenericStage> m_other, int portIdx) override
	{
		using port_type = output_type;

		auto port = std::unique_ptr<Port<port_type>>(new Port<port_type>);
		auto other = std::dynamic_pointer_cast<InputSide<output_type>>(m_other);
		if(other == nullptr)
		{
			throw std::runtime_error("input/output type mismatch.");
		}
		port->attach(other, portIdx);
		this->attach(std::move(port));
	}

	private:
	bool m_done;
	std::uint32_t m_num;
	std::string m_path;
	std::string m_parameterSet;
	std::mutex m_;
	std::condition_variable m_cv;
};
}
}

#endif /* PIPELINE_SOURCESTAGE_H_ */
