// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_OUTPUTSIDE_H_
#define PIPELINE_OUTPUTSIDE_H_

#include <memory>
#include <utility>

#include "Port.h"

namespace glados
{
namespace pipeline
{
template <class OutputType>
class OutputSide
{
	public:

	//! forward one item to a connected port.
	//! which Port is chosen is governed by outputIdx.
	auto output(OutputType&& out, int outputIdx = 0) -> void
	{
		if(m_ports[outputIdx] == nullptr)
			throw std::runtime_error("OutputSide: Missing port " + outputIdx);

		m_ports[outputIdx]->forward(std::forward<OutputType&&>(out));
	}

	//! connect an accepting Port to this output
	auto attach(std::unique_ptr<Port<OutputType>>&& port) noexcept -> void
	{
		m_ports.push_back(std::move(port));
	}

	protected:
	std::vector<std::unique_ptr<Port<OutputType>>> m_ports; //! Ports that this Output will send to
};
}
}

#endif /* PIPELINE_OUTPUTSIDE_H_ */
