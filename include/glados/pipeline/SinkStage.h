// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_SINKSTAGE_H_
#define PIPELINE_SINKSTAGE_H_

#include <cstdint>
#include <string>
#include <utility>

#include <boost/log/trivial.hpp>

#include "../Volume.h"
#include "../observer/Subject.h"

#include "GenericStage.h"
#include "InputSide.h"

namespace glados
{
namespace pipeline
{
template <class ImageSaver>
class SinkStage
	: public ImageSaver
	, public InputSide<typename ImageSaver::input_type>
	, public GenericStage
{
	public:
	using input_type = typename ImageSaver::input_type;

	public:
	template <typename... Args>
	SinkStage(const std::string& config, const std::string& parameterSet, Args&&... args)
		: ImageSaver(std::forward<Args>(args)..., config, parameterSet), InputSide<input_type>()
	{
		this->m_input_queues = std::vector<glados::Queue<input_type>>(1);
		this->identifier = parameterSet;
	}

	auto run() -> void
	{
		glados::utils::setThreadName("sinkStage");
		while(true)
		{
			auto img = this->m_input_queues[0].take();
			if(img.valid())
			{
				ImageSaver::saveImage(std::move(img));
			}
			else
			{
				break; // poisonous pill
			}
		}
	}

	auto update(Subject* s) -> void { ImageSaver::update(s); }

	void connectTo(std::shared_ptr<GenericStage> m_other, int portIdx) override
	{
		BOOST_LOG_TRIVIAL(error) << "Cannot connect a sink stage to another stage. Requested to connect to "
								 << m_other << " at port " << portIdx;
		return; // cannot connect saver to more stages
	}

	private:
	std::string identifier;
};
}
}

#endif /* PIPELINE_SINKSTAGE_H_ */
