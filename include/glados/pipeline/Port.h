// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PIPELINE_PORT_H_
#define PIPELINE_PORT_H_

#include <memory>
#include <utility>

#include "InputSide.h"

namespace glados
{
namespace pipeline
{
template <class DataType>
class Port
{
	public:

	//! forward the data to the next stage, using m_inputIdx to tell multiple InputQueues from one Stage apart
	void forward(DataType&& data) { m_next->input(std::forward<DataType&&>(data), m_inputIdx); }

	//! attach next to this port.
	//! inputIdx decides which inputQueue of next this port forwards its Data to.
	void attach(std::shared_ptr<InputSide<DataType>> next, int inputIdx) noexcept
	{
		m_next = next;
		m_inputIdx = inputIdx;
	}

	private:
	std::shared_ptr<InputSide<DataType>> m_next; //! Destination of this Port, this is the Input-Part of a Stage (such Stages will inherit from glados::pipeline::InputSide)
	int m_inputIdx; //! index of the InputQueue in the vector<Queue> of the InputSide::m_input_queues, this is used to tell the ports of multiple InputQueues of one Stage apart
};
}
}

#endif /* PIPELINE_PORT_H_ */
