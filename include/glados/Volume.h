// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_VOLUME_H_
#define GLADOS_VOLUME_H_

#include <algorithm>
#include <stdexcept>
#include <utility>

#include "Image.h"

namespace glados
{
template <class MemoryManager>
class Volume : public MemoryManager
{
	public:
	using value_type = typename MemoryManager::value_type;
	using pointer_type = typename MemoryManager::pointer_type_3D;
	using pointer_type_2D = typename MemoryManager::pointer_type_2D;
	using size_type = typename MemoryManager::size_type;

	public:
	Volume() noexcept : MemoryManager(), m_width{0}, m_height{0}, m_depth{0}, m_data{nullptr}, m_valid{false} {}

	Volume(size_type w, size_type h, size_type d, pointer_type ptr = nullptr)
		: MemoryManager(), m_width{w}, m_height{h}, m_depth{d}, m_data{std::move(ptr)}, m_valid{true}
	{
		if(m_data == nullptr)
			m_data = MemoryManager::make_ptr(m_width, m_height, m_depth);
	}

	Volume(const Volume& other)
		: MemoryManager()
		, m_width{other.m_width}
		, m_height{other.m_height}
		, m_depth{other.m_depth}
		, m_valid{other.m_valid}
	{
		if(other.m_data == nullptr)
			m_data = nullptr;
		else
		{
			m_data = MemoryManager::make_ptr(m_width, m_height, m_depth);
			MemoryManager::copy(m_data, other.m_data, m_width, m_height, m_depth);
		}
	}

	template <typename U>
	auto operator=(const Volume<U>& rhs) -> Volume<MemoryManager>&
	{
		m_width = rhs.width();
		m_height = rhs.height();
		m_depth = rhs.depth();

		if(rhs.container() == nullptr)
			m_data = nullptr;
		else
		{
			m_data = MemoryManager::make_ptr(m_width, m_height, m_depth);
			MemoryManager::copy(m_data, rhs.container(), m_width, m_height, m_depth);
		}

		return *this;
	}

	Volume(Volume&& other) noexcept
		: MemoryManager(std::move(other))
		, m_width{other.m_width}
		, m_height{other.m_height}
		, m_depth{other.m_depth}
		, m_data{std::move(other.m_data)}
		, m_valid{other.m_valid}
	{
	}

	auto operator=(Volume&& rhs) noexcept -> Volume&
	{
		MemoryManager::operator=(std::move(rhs));

		m_width = rhs.m_width;
		m_height = rhs.m_height;
		m_depth = rhs.m_depth;
		m_data = std::move(rhs.m_data);
		m_valid = rhs.m_valid;

		rhs.m_valid = false;
		return *this;
	}

	auto width() const noexcept -> size_type { return m_width; }

	auto height() const noexcept -> size_type { return m_height; }

	auto depth() const noexcept -> size_type { return m_depth; }

	auto pitch() const noexcept -> size_type { return m_data.pitch(); }

	auto data() const noexcept -> value_type* { return m_data.get(); }

	auto valid() const noexcept -> bool { return m_valid; }

	auto container() const noexcept -> const pointer_type& { return m_data; }

	auto operator[](size_type i) -> Image<MemoryManager>
	{
		if(i >= m_depth)
			throw std::out_of_range{"Volume: invalid slice index"};

		auto sliceIdx = i * m_width * m_height;
		auto slicePtr = m_data.get() + sliceIdx;
		auto ptr = MemoryManager::make_ptr(m_width, m_height);

		std::copy(slicePtr, slicePtr + m_width * m_height, ptr.get());

		// MemoryManager::copy(ptr, slicePtr, m_width, m_height);
		return Image<MemoryManager>{m_width, m_height, i, std::move(ptr)};
	}

	private:
	size_type m_width;
	size_type m_height;
	size_type m_depth;
	pointer_type m_data;
	bool m_valid;
};
}

#endif /* VOLUME_H_ */
