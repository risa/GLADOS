// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef MEMORY_H_
#define MEMORY_H_

#include <algorithm>
#include <cstddef>
#include <memory>

#include "../Memory.h"

namespace glados
{
namespace def
{
class copy_policy
{
	protected:
	~copy_policy() = default;

	/* 1D copies */
	template <class Dest, class Src>
	inline auto copy(Dest& dest, const Src& src, std::size_t size) -> void
	{
		std::copy(src.get(), src.get() + size, dest.get());
	}

	/* 2D copies */
	template <class Dest, class Src>
	inline auto copy(Dest& dest, const Src& src, std::size_t width, std::size_t height) -> void
	{
		std::copy(src.get(), src.get() + width * height, dest.get());
	}

	/* 3D copies */
	template <class Dest, class Src>
	inline auto copy(Dest& dest, const Src& src, std::size_t width, std::size_t height, std::size_t depth)
		-> void
	{
		std::copy(src.get(), src.get() + width * height * depth, dest.get());
	}
};

template <class T>
using ptr = glados::ptr<T, copy_policy, std::unique_ptr<T[]>>;
template <class T, class is3D>
using pitched_ptr = glados::pitched_ptr<T, copy_policy, is3D, std::unique_ptr<T[]>>;
}
}

#endif /* MEMORY_H_ */
