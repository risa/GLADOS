// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef DEF_MEMORYMANAGER_H_
#define DEF_MEMORYMANAGER_H_

#include <algorithm>
#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>

#include "Memory.h"

namespace glados
{
namespace def
{
template <class T>
class MemoryManager
{
	public:
	using value_type = T;
	using size_type = std::size_t;
	using pointer_type_1D = ptr<T>;
	using pointer_type_2D = pitched_ptr<T, std::false_type>;
	using pointer_type_3D = pitched_ptr<T, std::true_type>;

	public:
	inline auto make_ptr(size_type size) -> pointer_type_1D
	{
		auto ptr = std::unique_ptr<T[]>(new value_type[size]);
		return ptr<T, std::false_type>(std::move(ptr), size * sizeof(T));
	}

	inline auto make_ptr(size_type width, size_type height) -> pointer_type_2D
	{
		auto ptr = std::unique_ptr<T[]>(new value_type[width * height]);
		return pitched_ptr<T, std::false_type>(std::move(ptr), width * sizeof(T), width, height);
	}

	inline auto make_ptr(size_type width, size_type height, size_type depth) -> pointer_type_3D
	{
		auto ptr = std::unique_ptr<T>(new value_type[width * height * depth]);
		return pitched_ptr<T, std::true_type>(std::move(ptr), width * sizeof(T), width, height, depth);
	}

	inline auto copy(pointer_type_1D& dest, const pointer_type_1D& src, size_type size) -> void
	{
		std::copy(src.get(), src.get() + size, dest.get());
	}

	inline auto copy(pointer_type_2D& dest, const pointer_type_2D& src, size_type width, size_type height)
		-> void
	{
		std::copy(src.get(), src.get() + width * height, dest.get());
	}

	inline auto copy(pointer_type_3D& dest, const pointer_type_3D& src, size_type width, size_type height,
		size_type depth) -> void
	{
		std::copy(src.get(), src.get() + width * height * depth, dest.get());
	}
};
}
}

#endif /* DEF_MEMORYMANAGER_H_ */
