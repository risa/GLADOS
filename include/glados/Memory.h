// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_MEMORY_H_
#define GLADOS_MEMORY_H_

#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

/**
 * ddRF provides two types of pointers: A simple ("one-dimensional") one and a pitched ("multi-dimensional")
 * one. Implementations for different hardware have to provide CopyPolicy and Ptr for the underlying
 * operations.
 */

namespace glados
{
template <class T, class CopyPolicy, class Ptr>
class base_ptr : public CopyPolicy
{
	public:
	using pointer = typename Ptr::pointer;
	using element_type = typename Ptr::element_type;
	using deleter_type = typename Ptr::deleter_type;
	using underlying_type = Ptr;

	public:
	constexpr base_ptr() noexcept : m_ptr{} {}

	constexpr base_ptr(std::nullptr_t p) noexcept : m_ptr{p} {}

	base_ptr(Ptr ptr) noexcept : m_ptr{std::move(ptr)} {}

	base_ptr(base_ptr&& other) noexcept : m_ptr{std::move(other.m_ptr)} {}

	~base_ptr() = default;

	inline auto operator=(base_ptr&& r) noexcept -> base_ptr&
	{
		m_ptr = std::move(r.m_ptr);
		return *this;
	}

	inline auto release() noexcept -> pointer { return m_ptr.release(); }
	inline auto reset(pointer ptr = pointer()) noexcept -> void { m_ptr.reset(ptr); }
	template <class U>
	auto reset(U) noexcept -> void = delete;
	inline auto reset(std::nullptr_t p) noexcept -> void { m_ptr.reset(p); }
	inline auto swap(base_ptr& other) noexcept -> void { m_ptr.swap(other.m_ptr); }
	inline auto get() const noexcept -> pointer { return m_ptr.get(); }
	inline auto get_deleter() noexcept -> deleter_type { return m_ptr.get_deleter(); }
	inline auto get_deleter() const noexcept -> const deleter_type { return m_ptr.get_deleter(); }
	explicit inline operator bool() const noexcept { return m_ptr.operator bool(); }
	inline auto operator[](std::size_t i) const -> element_type& { return m_ptr[i]; }

	// this is nasty but currently I see no other way to access the m_ptr of different instances
	inline const Ptr& get_underlying() const noexcept { return m_ptr; }

	protected:
	template <class Dest, class Src, class... Args>
	inline auto copy(Dest& dest, const Src& src, Args... args) -> void
	{
		CopyPolicy::copy(dest, src, args...);
	}

	private:
	base_ptr(const base_ptr&) = delete;
	auto operator=(const base_ptr&) -> base_ptr& = delete;

	protected:
	Ptr m_ptr;
};

template <class T1, class C1, class Ptr1, class T2, class C2, class Ptr2>
inline auto operator==(const base_ptr<T1, C1, Ptr1>& x, const base_ptr<T2, C2, Ptr2>& y) noexcept -> bool
{
	return x.get() == y.get();
}

template <class T1, class C1, class Ptr1, class T2, class C2, class Ptr2>
inline auto operator!=(const base_ptr<T1, C1, Ptr1>& x, const base_ptr<T2, C2, Ptr2>& y) noexcept -> bool
{
	return x.get() != y.get();
}

template <class T, class C, class Ptr>
inline auto operator==(const base_ptr<T, C, Ptr>& x, std::nullptr_t) noexcept -> bool
{
	return !x;
}

template <class T, class C, class Ptr>
inline auto operator==(std::nullptr_t, const base_ptr<T, C, Ptr>& x) noexcept -> bool
{
	return !x;
}

template <class T, class C, class Ptr>
inline auto operator!=(const base_ptr<T, C, Ptr>& x, std::nullptr_t) noexcept -> bool
{
	return static_cast<bool>(x);
}

template <class T, class C, class Ptr>
inline auto operator!=(std::nullptr_t, const base_ptr<T, C, Ptr>& x) noexcept -> bool
{
	return static_cast<bool>(x);
}

template <class T, class CopyPolicy, class Ptr>
class ptr : public base_ptr<T, CopyPolicy, Ptr>
{
	private:
	using base = base_ptr<T, CopyPolicy, Ptr>;
	std::size_t m_size;

	public:
	using pointer = typename base::pointer;
	using element_type = typename base::element_type;
	using deleter_type = typename base::deleter_type;
	using underlying_type = typename base::underlying_type;
	static constexpr auto has_pitch = false;

	public:
	constexpr ptr() noexcept : base(), m_size{0u} {}

	constexpr ptr(std::nullptr_t p) noexcept : base(p), m_size{0u} {}

	ptr(Ptr p, std::size_t s) noexcept : base(std::move(p)), m_size{s} {}

	ptr(ptr&& other) noexcept : base(std::move(other)), m_size{other.m_size} {}

	~ptr() = default;

	inline auto operator=(ptr&& r) noexcept -> ptr&
	{
		m_size = r.m_size;
		base::operator=(std::move(r));
		return *this;
	}

	inline auto size() const noexcept -> std::size_t { return m_size; }

	private:
	ptr(const ptr&) = delete;
	auto operator=(const ptr&) -> ptr& = delete;
};

template <class T, class CopyPolicy, class is3D, class Ptr>
class pitched_ptr : public base_ptr<T, CopyPolicy, Ptr>
{
	private:
	using base = base_ptr<T, CopyPolicy, Ptr>;

	public:
	using pointer = typename base::pointer;
	using element_type = typename base::element_type;
	using deleter_type = typename base::deleter_type;
	using underlying_type = typename base::underlying_type;

	static constexpr auto has_pitch = true;
	static constexpr auto is3DPtr = is3D::value;

	public:
	constexpr pitched_ptr() noexcept : base(), m_pitch{0u}, m_width{0u}, m_height{0u}, m_depth{0u} {}

	constexpr pitched_ptr(std::nullptr_t p) noexcept
		: base(p), m_pitch{0u}, m_width{0u}, m_height{0u}, m_depth{0u}
	{
	}

	pitched_ptr(Ptr ptr, std::size_t p, std::uint32_t w, std::uint32_t h, std::uint32_t d = 0) noexcept
		: base(std::move(ptr)), m_pitch{p}, m_width{w}, m_height{h}, m_depth{d}
	{
	}

	pitched_ptr(pitched_ptr&& other) noexcept
		: base(std::move(other))
		, m_pitch{other.m_pitch}
		, m_width{other.m_width}
		, m_height{other.m_height}
		, m_depth{other.m_depth}
	{
	}

	~pitched_ptr() = default;

	auto operator=(pitched_ptr&& r) noexcept -> pitched_ptr&
	{
		m_pitch = r.m_pitch;
		m_width = r.m_width;
		m_height = r.m_height;
		m_depth = r.m_depth;
		base::operator=(std::move(r));

		return *this;
	}

	inline auto pitch() const noexcept -> std::size_t { return m_pitch; }
	inline auto width() const noexcept -> std::size_t { return m_width; }
	inline auto height() const noexcept -> std::size_t { return m_height; }
	inline auto depth() const noexcept -> std::size_t { return m_depth; }

	private:
	pitched_ptr(const pitched_ptr&) = delete;
	auto operator=(const pitched_ptr&) -> pitched_ptr& = delete;

	private:
	std::size_t m_pitch;
	std::size_t m_width;
	std::size_t m_height;
	std::size_t m_depth;
};
}

#endif /* GLADOS_MEMORY_H_ */
