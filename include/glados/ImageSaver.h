// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_IMAGESAVER_H_
#define GLADOS_IMAGESAVER_H_

#include <string>
#include <utility>

#include "Image.h"
#include "Volume.h"

namespace glados
{
template <class Implementation>
class ImageSaver : public Implementation
{
	public:
	using input_type = typename Implementation::input_type;

	public:
	ImageSaver(const std::string& path, const std::string& parameterSet) : Implementation(path, parameterSet)
	{
	}

	/*
	 * Saves an image to the given path.
	 */
	auto saveImage(input_type image) -> void { Implementation::saveImage(std::move(image)); }

	/*
	 * Saves a volume to the given path.
	 */
	/*auto saveVolume(Volume<manager_type> volume, std::string path) -> void
	{
		Implementation::saveVolume(std::move(volume), path);
	}*/

	protected:
	~ImageSaver() = default;
};
}

#endif /* GLADOS_IMAGESAVER_H_ */
