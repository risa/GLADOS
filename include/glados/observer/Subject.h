// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef GLADOS_SUBJECT_H_
#define GLADOS_SUBJECT_H_

#include <list>

#include "Observer.h"

namespace glados
{
class Subject
{
	public:
	virtual ~Subject() = default;

	virtual auto attach(Observer*) -> void;
	virtual auto detach(Observer*) -> void;
	virtual auto notify() -> void;

	protected:
	Subject() = default;

	private:
	std::list<Observer*> m_observers;
};
}

#endif /* SUBJECT_H_ */
