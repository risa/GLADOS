// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef FILEOBSERVER_LINUX_H_
#define FILEOBSERVER_LINUX_H_

#include <boost/log/trivial.hpp>
#include <future>
#include <glados/observer/Subject.h>
#include <list>
#include <memory>
#include <string>
#include <sys/inotify.h>
#include <sys/types.h>
#include <thread>
#include <vector>

#define m_MAX_NUM_EVENTS 1024
#define MAX_FILE_NAME_LEN 32
#define m_EVENT_SIZE (sizeof(struct inotify_event))
#define m_EVENT_BUF_LEN (m_MAX_NUM_EVENTS * (m_EVENT_SIZE + MAX_FILE_NAME_LEN))

namespace glados
{
//! This class implements subject type `FileObserver`.
/**
 * Combinations of "Subject" and "Observer" are used to enable runtime async messages to the stages,
 * e.g. for dynamic reconfiguration. Every stage has the capabilty to "observere" changes of a
 * "Subject". In this class, the Subject implementation for FileObserver is provided.
 *
 * The FileObserver class monitors changes to a file set by "setFilePathToObserve". Using the
 * syscall inotify, the programm will be notified about any modifications of the monitored file.
 * The FileObserver class will then "notify" all the "observers" (i.e. Stages) which are attached
 * to it about this change in the file. The Stage implementation of "update" will be called such
 * that the Stage can update given the new file contents.
 */

class FileObserver : public glados::Subject
{
	public:
	FileObserver() : Subject(){};

	//! intializes the FileObserver
	auto init(glados::Observer* o, std::string filePath, const std::string& fileName, uint32_t inotifyEvents) -> void;

	//! attach an observer to the FileObserver
	auto attach(glados::Observer* o) -> void;

	//! detach an observer from the FileObserver
	auto detach(glados::Observer* o) -> void;

	//! notify all attached observers
	auto notify() -> void;

	//! set file path and name to monitor
	/**
	 * @retval true file monitoring successfully initialized
	 * @retval false error when initializing file monitoring
	 */
	auto setFilePathToObserve(std::string filePath, const std::string& fileName) -> bool;

	//! Set the inotify event types to observe
	/*
	 * Examples: IN_ACCESS, IN_CREATE, IN_DELETE, IN_MODIFY, IN_OPEN
	 *
	 * for details, see: https://man7.org/linux/man-pages/man7/inotify.7.html
	 *
	 */
	auto setEventsToObserve(uint32_t inotifyEvents) -> void;

	//! start the FileObserver
	auto start() -> void;
	auto wait() -> void;

	// notified observers can access all public class members:
	std::string m_message;
	std::string m_filePath, m_fileName;
	std::vector<char> m_fileContents;
	bool m_fileContentsValid;
	bool m_initOk;

	auto observe(std::future<void> stopSig) -> void;
	std::thread m_observerThread;
	std::list<glados::Observer*> m_observers;
	int m_watchDescriptor; //! inotify watch descriptor of the file
	int m_inotifyInstance; //! inotify instance
	uint32_t m_observedEvents; //! bitmask of inotify events to observe
	std::promise<void> m_stopSig;
};
}

#endif /* FILEOBSERVER_LINUX_H_ */
