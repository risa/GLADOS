// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef CIRCULARBUFFER_H_
#define CIRCULARBUFFER_H_

#include <cstddef>
#include <vector>

namespace glados
{

template <class Object>
class CircularBuffer
{
	public:
	CircularBuffer(std::size_t size) : m_size{size}, m_count{0u}, m_index{0u}, m_buffer{std::vector<Object>(size)}
	{
	}

	template <class Item>
	void push_back(Item&& item)
	{
		if(m_count < m_size)
			m_count = m_index + 1u;
		// making Image go out of scope(otherwise performance drop if buffer filled)
		auto release = std::move(m_buffer[m_index]);
		m_buffer[m_index] = std::move(item);
		m_index = (m_index + 1) % m_size;
	}

	Object at(std::size_t index)
	{
		auto ret = std::move(m_buffer[index]);
		return ret;
	}

	std::size_t count() { return m_count; }

	bool full() { return m_count >= m_size; }

	void clear()
	{
		m_count = 0u;
		m_index = 0u;
	}

	private:
	const std::size_t m_size;
	std::size_t m_count;
	std::size_t m_index;
	std::vector<Object> m_buffer;
};

}

#endif /* CIRCULARBUFFER_H_ */
